import { Component, ViewChild, ElementRef, AfterViewInit, HostListener, Inject } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'etailx';
  showMobileView: boolean;
  constructor(public route: Router) {
    this.route.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (window as any).ga('set', 'page', event.urlAfterRedirects);
        (window as any).ga('send', 'pageview');
        (window as any).fbq('track', 'PageView');
      }
    });
  }
  ngAfterViewInit(){
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  // show(){
  //   this.showMobileView = !this.showMobileView;
  // }


}
