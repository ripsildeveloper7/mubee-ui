import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { SigninDailogComponent } from './signin-dailog.component';
import { take, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SigninDialogService {
  dialogRef: MatDialogRef<SigninDailogComponent>;
  constructor(private dialog: MatDialog) { }

  openPopup(signin?: any): Observable<boolean> {

    this.dialogRef = this.dialog.open(SigninDailogComponent,
      {
        disableClose: true, 
        width: 'auto',
        height: 'auto',
        maxWidth: '720px',
        panelClass: 'c1',
        data: signin
      });
      return this.dialogRef.afterClosed();
  }
  closePopup() {
    this.dialogRef.close();
  }
}
