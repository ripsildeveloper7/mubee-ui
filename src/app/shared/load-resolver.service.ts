import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router'
 
@Injectable({
  providedIn: 'root'
})
export class LoadResolverService implements Resolve<any> {

  constructor() { }

  resolve(){
    console.log("hello");
  }
}
