export class SubCategory {
    _id: string;
    subCategoryName: string;
    subCategoryDescription: string;
    metaTagTitle: string;
    metaTagDescription: string;
    metaTagKeyword: string;
    count: number;
}
