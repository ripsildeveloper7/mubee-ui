import { CardDetailModel } from './cardDetails.model';
import { AddressModel } from './address.model';
import { ProfileModel } from './profile.model';
export class RegModel {
    mobileNumber: number;
    emailId: string;
    password: string;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    location: string;
    gender: string;
    addressDetails: [AddressModel];
    cardDetails: [CardDetailModel];
    result: string;
    /* ProfileModels: [ProfileModel]; */
}
