import { Pipe, PipeTransform } from '@angular/core';

import {Rates} from './rates.model';

@Pipe({
  name: 'converter'
})
export class ConverterPipe  implements PipeTransform {
  /* rates: object;
  rateArray: Array<string>;
  index: number;
  selected: number; */
  rates: object;
  rateArray = [];
  index = 0;
  notindex = 0;
  selected = 0;
  notselected = 0;
  dataConversion = 0;
  brakfalse = false;
  transform(value: any, args?: any): any {
    // Get rates Object from rates.ts
    this.rates = new Rates().rates;
    // Get all the keys i.e the currency names
    this.rateArray = Object.keys(this.rates);
    // Search the index of the selected currency name in array
    this.index = this.rateArray.indexOf('INR');
     // Get the value of the selected currency i.e rate of selected currency from the object
    this.notindex = this.rateArray.indexOf('USD');
    this.selected = this.rates[Object.keys(this.rates)[this.index]];
    this.notselected = this.rates[Object.keys(this.rates)[this.notindex]];
    return ((this.notselected / this.selected * value).toFixed(2));
  }

}
