import { Component, OnInit } from '@angular/core';
import {SharedService} from '../shared.service';
import {Footer} from './footer.model';
import {Header} from '../nav/header.model';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  footerDetails: Footer;
  header: Header[];
  imageData: any;
  showMobileView = false;
  superCategory: any;
  validEmail = true;
  message:any;
  action:any;

  constructor(private sharedService: SharedService,private snackBar:MatSnackBar, private router: Router) { 
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
   }

  ngOnInit() {
    this.allFooter();
    this.getLogo();
    this.getSuperCategory();
}


getLogo() {
  this.sharedService.getHeaderImage().subscribe(data => {
    /* console.log(data); */
    this.imageData = data[0];
  }, error => {
    console.log(error);
  });
}
allFooter() {
  this.sharedService.getFooterDetails().subscribe(data => {
    this.footerDetails = data[0];
    console.log(data);
  }, error => {
    console.log(error);
  });
}
ngAfterViewInit(){
  this.getWindowSize();
}
getWindowSize() {
  if (window.screen.width > 900) {
    this.showMobileView = false;
  } else {
    this.showMobileView = true;
  }
}
// getSuperCategory() {
//   this.sharedService.getSuperCategory().subscribe(data => {
//     this.superCategory = data;
//     this.allFooter();
//   });
// }
getSuperCategory() {
  this.sharedService.getSuperCategory().subscribe(data => {
    this.superCategory = data;
    
    console.log(this.superCategory,"sup");
    this.allFooter();
  });
}
getCategory(cat) {
  console.log(cat);
  if (cat.mainCategory.length === 0) {
    this.router.navigate(['product/supercategory/', cat._id]);
  } else {
    this.router.navigate(['product/categorylandingpage/', cat._id]);
  }
}

submit(val){
this.sharedService.addSubscriber(val).subscribe(data =>{
console.log(data);
})
}

  subscribe(val){
    let email = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
    if(val.match(email)){
      this.validEmail = true;
      this.message = "subscribed succesfully"
      this.sharedService.addSubscriber(val).subscribe(data =>{
        console.log(data);
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
        })

    } else {
      this.validEmail = false;
      console.log("Invalid");
    }
    
    }

}
