import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NoopAnimationsModule , BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule , HttpClientJsonpModule} from '@angular/common/http';
import { MatIconModule} from '@angular/material';
import { registerLocaleData } from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { MatBadgeModule } from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import { LOCALE_ID } from '@angular/core';
import localeEs from '@angular/common/locales/en-IN';

registerLocaleData(localeEs, 'en-IN');


@NgModule({
  declarations: [
    AppComponent
   
  ],
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    MatIconModule,
    MatBadgeModule,
    MatButtonModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-IN' }],
  bootstrap: [AppComponent],
})
export class AppModule { }
