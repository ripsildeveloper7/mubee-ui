import { Component, OnInit } from '@angular/core';
import { InfoService } from '../info.service';
import { AppSetting } from 'src/app/config/appSetting';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  imageUrl: any;
  aboutUs : any;
  aboutUs1: any;
  constructor( private infoservice: InfoService) {
    this.infoservice.getAboutUs()
    .subscribe( data => {
      this.aboutUs = data[0];
      this.aboutUs1 = data[1];
      console.log(this.aboutUs);
    })
   }

  ngOnInit() {
    // this.imageUrl = AppSetting.aboutUsImageUrl;
  }

}
