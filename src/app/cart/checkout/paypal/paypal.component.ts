import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
declare var paypal;

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.css']
})
export class PaypalComponent implements OnInit {
  @ViewChild('paypal', {static: true}) paypalElement: ElementRef;

  constructor() { }

  ngOnInit() {
    paypal.Buttons({
     /*  createOrder: (data, actions) => {
        return actions.order.create({
          purchase_units: [{
            description: 'Sample',
            amount: {
              currency_code: 'USD',
              value: 1
            }
          }]
        });
      } */
    }).render(this.paypalElement.nativeElement);
  }
  test() {
    console.log('test');
  }
}

