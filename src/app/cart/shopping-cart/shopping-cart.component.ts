import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CartService } from './../cart.service';

import { Product } from '../../shared/model/product.model';
import { Cart } from './../../shared/model/cart.model';

import { WishList } from './../../shared/model/wishList.model';
import { Router } from '@angular/router';
import { AppSetting } from '../../config/appSetting';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { match } from 'minimatch';
import { SigninDailogComponent } from 'src/app/shared/signin-dailog/signin-dailog.component';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit, AfterViewInit {

  show = false;

  wish: WishList;
  wishList: WishList[];
  shoppinBagForm: FormGroup;
  shopModel: any = [];
  cartModel: Cart;
  userId;
  subTotal = 0;
  action;
  localImageUrlView = true;
  // showMobileView = false;
  totalItems = 0;
  noPrductAdd = false;
  productImageUrl: string = AppSetting.productImageUrl;
  discountStore: any;
  checkPoint1 = false;
  checkPoint2 = false;
  checkPoint3 = false;
  checkPoint4 = false;
  checkPoint5 = false;
  checkPoint6 = false;
  checkPoint7 = false;
  checkPoint8 = false;
  checkPoint9 = false;
  displayMobile = false;
  isFirstTimeCoupon = false;
  isFlexibleCoupon = false;
  firstTimeCoupon: any;
  flexibleCoupon: any;
  message;
  customerData: {
    customerId: string,
    couponCode: string
  };
  couponType;
  couponValue;
  totalAmount = 0;
  serviceData: any;
  constructor(private dialog: MatDialog, private cartService: CartService, private router: Router,
    private matSnackBar: MatSnackBar, private fb: FormBuilder) {

  }

  ngOnInit() {
    this.createForm();
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.localImageUrlView = true;
      this.shoppingCartUser(this.userId);
    } else {
      this.localImageUrlView = false;
      this.shopModel = JSON.parse(sessionStorage.getItem('cart')) || [];
      this.shopModel.forEach((val) => {
        val.showDiv = false;
      });
      console.log('cart', this.shopModel);

      /* this.getDiscount(); */
      this.total();
    }
    this.getFirstTimeSignUpCoupon();
    this.getFlexibleCoupon();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }
  discountCalculation() {
    for (const cart of this.shopModel) {
      for (const product of cart.cart_product[0].child) {
        const discount = 100 - product.discount;
        const totalPrice = product.sp * (100 / discount);
        const savePrice = totalPrice - product.sp;
        product.savePrice = savePrice;
        product.totalPrice = totalPrice;
      }
    }
    // this.getAllReadyToWear();
  }
  getAllReadyToWear() {
    this.cartService.getAllReadyToWear().subscribe(data => {
      this.serviceData = data;
      this.checkService();
    }, error => {
      console.log(error);
    });
  }
  checkService() {
    for (const cart of this.shopModel) {
      if (cart.items.tailoringService === true) {
        this.serviceData.forEach(element => {
          if (element._id === cart.items.serviceId) {
            for (const product of cart.cart_product[0].child) {
              product.serviceActive = true;
              product.serviceType = 'Ready to Wear';
              product.serviceId = element._id;
              product.serviceName = element.serviceName;
              product.serviceAmount = element.price;
              product.serviceDiscount = element.discount;
              product.price = product.sp + element.price;
            }
          }
        });
      } else {
        continue;
      }
    }
    this.total();
  }
  createForm() {
    this.shoppinBagForm = this.fb.group({
      promoCode: ['']
    });
  }
  orderPlaced() {
    this.matSnackBar.open('order Placed Successfully', this.action, {
      duration: 2000,
    });
    this.router.navigate(['home/welcome']);
  }

  actionPlus(product, INTsku) {
    if (this.localImageUrlView) {
      this.actionServerPlus(product, INTsku);
    } else {
      this.actionLocalPlus(product, INTsku);
    }
  }
  actionServerPlus(product, selectedCart) {
    this.userId = sessionStorage.getItem('userId');
    // if (selectedCart.tailoringService === true) {
    //   const totalItem: any = [];
    //   const cart: any = {
    //     productId: product,
    //     INTsku: selectedCart.INTsku,
    //     qty: 1,
    //     tailoringService: true,
    //     selectedSize: selectedCart.selectedSize,
    //     serviceId: selectedCart.serviceId
    //   };
    //   totalItem.push(cart);
    //   this.cartModel = new Cart();
    //   this.cartModel.userId = this.userId;
    //   this.cartModel.items = totalItem;
    // } else {
    //   const totalItem: any = [];
    //   const cart: any = {
    //     productId: product,
    //     INTsku: selectedCart.INTsku,
    //     qty: 1,
    //     tailoringService: false
    //   };
    //   totalItem.push(cart);
    //   this.cartModel = new Cart();
    //   this.cartModel.userId = this.userId;
    //   this.cartModel.items = totalItem;
    // }
    const totalItem: any = [];
    const cart: any = {
          productId: product,
          INTsku: selectedCart,
          qty: 1,
          selectedSize: selectedCart.selectedSize,
          serviceId: selectedCart.serviceId
        };
        totalItem.push(cart);
        this.cartModel = new Cart();
        this.cartModel.userId = this.userId;
        this.cartModel.items = totalItem;
    
    this.cartService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      this.total();
      /* this.getDiscount(); */
    }, error => {
      console.log(error);
    });
  }
  actionLocalPlus(item, selectedCart) {
    // if (selectedCart.tailoringService === true) {
    //   const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.selectedSize === selectedCart.selectedSize);
    //   localSame.items.qty++;
    // } else {
    //   const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.tailoringService === false);  
    //   localSame.items.qty++;
    // }
     const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart); 
    //  const localSame = this.shopModel.find(s => s.items.INTsku === sku);
    localSame.items.qty++; 
    sessionStorage.setItem('cart', JSON.stringify(this.shopModel));
    this.total();
    /* this.getDiscount(); */
  }

  actionMinus(product, sku) {
    console.log(sku);
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.actionServerMinus(product, sku);
    } else {
      this.actionLocalMinus(product, sku);
    }
  }
  continueShopping() {
    this.router.navigate(['product/viewproduct']);
  }
  actionServerMinus(product, selectedCart) {
    this.userId = sessionStorage.getItem('userId');
    // if (selectedCart.tailoringService === true) {
    //   const cart: any = {
    //     productId: product,
    //     INTsku: selectedCart.INTsku,
    //     qty: 1,
    //     tailoringService: true,
    //     selectedSize: selectedCart.selectedSize,
    //     serviceId: selectedCart.serviceId
    //   };
    //   this.cartModel = new Cart();
    //   this.cartModel.userId = this.userId;
    //   this.cartModel.items = cart;
    // } else {
    //   const cart: any = {
    //     productId: product,
    //     INTsku: selectedCart.INTsku,
    //     qty: 1,
    //     tailoringService: false,
    //   };
    //   this.cartModel = new Cart();
    //   this.cartModel.userId = this.userId;
    //   this.cartModel.items = cart;
    // }

    
    const cart: any = {
          productId: product,
          INTsku: selectedCart,
          qty: 1,
          selectedSize: selectedCart.selectedSize,
          serviceId: selectedCart.serviceId
        };
      
        this.cartModel = new Cart();
        this.cartModel.userId = this.userId;
        this.cartModel.items = cart;
    
    this.cartService.addToCartDecrement(this.cartModel).subscribe(data => {
      this.shopModel = data;
      /* this.getDiscount(); */
      this.total();
    }, error => {
      console.log(error);
    });
  }

  actionLocalMinus(product, selectedCart) {
    // if (selectedCart.tailoringService === true) {
    //   const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.serviceId === selectedCart.serviceId && s.items.selectedSize === selectedCart.selectedSize);
    //   localSame.items.qty--;
    // } else {
    //   const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart.INTsku && s.items.tailoringService === false);
    //   localSame.items.qty--;
    // }
    const localSame = this.shopModel.find(s => s.items.INTsku === selectedCart);
    localSame.items.qty--;
    sessionStorage.setItem('cart', JSON.stringify(this.shopModel)); 
    
    
    /* this.getDiscount(); */
    this.total();
  }

  shoppingCartUser(userId) {
    this.cartService.shoppingUser(userId).subscribe(data => {
      this.shopModel = data;
      // this.shopModel.forEach(d => {
      //   d.items.INTsku = parseInt(d.items.INTsku);
      // })
      console.log('cart length', data);

      /* this.getDiscount(); */
      // this.discountCalculation();
      this.total();
    }, err => {
      console.log(err);
    });
  }
  removeCart(item, sku) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.removeServerCart(item);
    } else {
      this.removeLocalCart(sku);
    }
  }
  removeLocalCart(selectedCart) {
    const item = this.shopModel.find(ite => {
      return ite.items.INTsku === selectedCart.INTsku && ite.items.serviceId === selectedCart.serviceId && ite.items.selectedSize === selectedCart.selectedSize;
    });
    const index = this.shopModel.indexOf(item);
    this.shopModel.splice(index, 1);
    sessionStorage.setItem('cart', JSON.stringify(this.shopModel));
    this.shopModel = JSON.parse(sessionStorage.getItem('cart')) || [];
    sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
    /* this.getDiscount(); */
    this.total();
  }
  removeServerCart(item) {
    this.cartService.deleteToCart(this.userId, item).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
      /* this.getDiscount(); */
      this.total();
    }, err => {
      console.log(err);
    });
  }
  total() {
    if (sessionStorage.getItem('coupon')) {
      this.subTotal = 0;
      this.totalItems = 0;
      this.totalAmount = 0;
      const totalProduct: any = this.shopModel.map(item => item.cart_product[0]);
      const totalSet = this.shopModel.map(item => item.items);
      /* this.totalItems += totalSet.length; */
      this.shopModel.forEach(e => {
        this.totalItems += e.items.qty;
      });
      totalSet.map(item => {
        const priceSingle = totalProduct.find(test => test._id === item.productId);
        const priceSizeVariant = priceSingle.child;
        const priceSize = priceSizeVariant.find(check => check.INTsku === item.INTsku);
        this.subTotal += item.qty * priceSize.price;
      });
      this.totalAmount = this.subTotal;
      console.log(this.shopModel.length);
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
      this.checkCouponInExisting();
    } else {
      this.subTotal = 0;
      this.totalItems = 0;
      this.totalAmount = 0;
      const totalProduct: any = this.shopModel.map(item => item.cart_product[0]);
      const totalSet = this.shopModel.map(item => item.items);
      /* this.totalItems += totalSet.length; */
      this.shopModel.forEach(e => {
        this.totalItems += e.items.qty;
      });
      totalSet.map(item => {
        const priceSingle = totalProduct.find(test => test._id === item.productId);
        const priceSizeVariant = priceSingle.child;
        const priceSize = priceSizeVariant.find(check => check.INTsku === item.INTsku);
        this.subTotal += item.qty * priceSize.price;
      });
      this.totalAmount = this.subTotal;
      // console.log(this.shopModel.length);
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
    }
    this.discountCalculation();
  }
  placeOrder() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.router.navigate(['cart/checkout']);
    } else {
      // this.router.navigate(['account/acc/signin']);
      this.openDialog();
    }
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SigninDailogComponent, {
      panelClass: 'c1',

    });
  }
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.cartService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
    }, err => {
      console.log(err);
    });
  }
  checkLoginUser(proId, sku) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.moveToWish(proId, sku);
    } else {
      this.router.navigate(['/account/acc/signin']);
    }
  }
  moveToWish(proId, sku) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = proId;
    this.wish.INTsku = sku;
    this.cartService.moveWishlist(this.wish).subscribe(data => {
      this.shopModel = data;
      /* this.getDiscount(); */
      this.total();
    }, err => {
      console.log(err);
    });
  }

  getDiscount() {
    this.cartService.getAllDiscount().subscribe(data => {
      this.discountStore = data;

      this.discountTotal();
    }, error => {
      console.log(error);
    });
  }
  discountTotal() {
    this.subTotal = 0;
    this.totalItems = 0;
    this.totalItems = this.shopModel.map(ele => ele.items).length;
    console.log('discount', this.discountStore);
    for (let l = 0; l <= this.shopModel.length - 1; l++) {
      for (let m = 0; m <= this.shopModel[l].cart_product.length - 1; m++) {
        for (let n = 0; n <= this.shopModel[l].cart_product[m].child.length - 1; n++) {

          if (this.shopModel[l].cart_product[m].child[n].sku === this.shopModel[l].items.sku) {
            if (this.shopModel[l].cart_product[m].discount === undefined || this.shopModel[l].cart_product[m].discount === 0) {
              this.shopModel[l].cart_product[m].child[n].discount = 0;
              this.checkPoint1 = true;
              this.shopModel[l].cart_product[m].displayClass = 'discountNone';
              this.shopModel[l].cart_product[m].child[n].displayClass = 'discountNone';
            }
            for (let i = 0; i <= this.discountStore.length - 1; i++) {
              for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
                for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {
                  /* -------------------------------------------------------------------------------------------------------- */
                  if (this.discountStore[i].conditions[j].field === 'Product Name') {
                    if (this.shopModel[l].cart_product[m]._id === this.discountStore[i].conditions[j].value[k]) {
                      if (this.shopModel[l].cart_product[m].child[n].displayClass === 'discountStyle') {
                        if (this.discountStore[i].amountType === 'Flat') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint4 = true;
                            console.log('check point 8', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        } else if (this.discountStore[i].amountType === 'Percentage') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint5 = true;
                            console.log('check point 6', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        }
                      } else {
                        if (this.discountStore[i].amountType === 'Percentage') {
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.checkPoint2 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          console.log('check point 1', this.shopModel[l].cart_product[m].child[n].discount);
                        } else {
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.checkPoint3 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          console.log('check point 2', this.shopModel[l].cart_product[m].child[n].discount);
                        }
                      }
                    } else {
                      continue;
                    }
                  } else if (this.discountStore[i].conditions[j].field === 'Product Category') {
                    if (this.shopModel[l].cart_product[m].superCategoryId === this.discountStore[i].conditions[j].value[k]) {
                      if (this.shopModel[l].cart_product[m].child[n].displayClass === 'discountStyle') {
                        if (this.discountStore[i].amountType === 'Flat') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint8 = true;
                            console.log('check point 4', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        } else if (this.discountStore[i].amountType === 'Percentage') {
                          const temp = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                            this.shopModel[l].cart_product[m].child[n].discount = temp;
                            this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                            this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                            this.checkPoint9 = true;
                            console.log('check point 3', this.shopModel[l].cart_product[m].child[n].discount);
                          } else {
                            continue;
                          }
                        }
                      } else {
                        if (this.discountStore[i].amountType === 'Percentage') {
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.checkPoint6 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                          console.log('check point 1', this.shopModel[l].cart_product[m].child[n].discount);
                        } else {
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.checkPoint7 = true;
                          this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                          console.log('check point 2', this.shopModel[l].cart_product[m].child[n].discount);
                        }
                      }
                    }
                  }

                  /* ----------------------------------------------------------------------------------------------- */
                }
              }
            }
            if (this.checkPoint1 === true || this.checkPoint2 === true || this.checkPoint3 === true ||
              this.checkPoint6 === true || this.checkPoint7 === true) {
              console.log('check point 3', this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty);
              this.subTotal += this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty;
              this.checkPoint1 = false;
              this.checkPoint2 = false;
              this.checkPoint3 = false;
            }
          }


        }
      }
    }
    console.log('subTotal', this.subTotal);
    console.log('totalItem', this.totalItems);
    console.log('updated', this.shopModel);
  }
  getFirstTimeSignUpCoupon() {
    this.cartService.getFirstTimeSignUpCoupon().subscribe(data => {
      this.firstTimeCoupon = data;
      console.log('first time coupon', data);
    }, error => {
      console.log(error);
    });
  }
  getFlexibleCoupon() {
    this.cartService.getFlexibleCoupon().subscribe(data => {
      this.flexibleCoupon = data;
      console.log('flexible coupon', data);
    }, error => {
      console.log(error);
    });
  }
  checkLogInForCoupon(coupon) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.checkFirstTimeCoupon(coupon);
    } else {
      this.openDialog();
    }
  }
  checkFirstTimeCoupon(coupon) {
    if (this.firstTimeCoupon.length !== 0) {
      this.isFirstTimeCoupon = false;
      this.firstTimeCoupon.forEach(a => {
        if (a.couponCode === coupon.couponCode) {
          this.isFirstTimeCoupon = true;
          this.checkFirstTimeCouponValidate(a, coupon);
        }
        if (!this.isFirstTimeCoupon) {
          this.checkFlexibleCoupon(coupon);
        }
      });
    } else {
      this.checkFlexibleCoupon(coupon);
    }
  }
  checkFirstTimeCouponValidate(firstCoupon, coupon) {
    firstCoupon.conditions.forEach(element => {
      if (element.field === 'Order total') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.subTotal.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.subTotal.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.subTotal.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      } else if (element.field === 'Order quantity') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.totalItems.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.totalItems.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.totalItems.toString()) {
            this.checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      }
    });
  }
  checkFlexibleCoupon(coupon) {
    if (this.flexibleCoupon.length !== 0) {
      this.isFlexibleCoupon = false;
      this.flexibleCoupon.forEach(a => {
        if (a.couponCode === coupon.couponCode) {
          this.isFlexibleCoupon = true;
          this.checkFlexibleCouponValidation(a, coupon);
        }
      });
      if (!this.isFlexibleCoupon) {
        this.showInValidateCoupon();
      }
    } else {
      this.showInValidateCoupon();
    }
  }
  checkFirstTimeSignupCouponByCustomer(firstCoupon, coupon) {
    const userId = sessionStorage.getItem('userId');
    this.cartService.checkCouponForFirstCustomer(userId).subscribe(data => {
      console.log(data);
      if (data.length === 0) {
        this.couponCalculation(firstCoupon);
      } else {
        this.showInValidateCoupon();
      }
    }, error => {
      console.log(error);
    });
  }
  showInValidateCoupon() {
    sessionStorage.removeItem('coupon');
    this.totalAmount = this.subTotal;
    this.couponType = '';
    this.message = 'Invalid Coupon Code';
    this.matSnackBar.open(this.message, this.action, {
      duration: 1000,
      verticalPosition: 'top',
      panelClass: ['snackbar1']
    });
  }
  checkFlexibleCouponValidation(flexiCoupon, coupon) {
    flexiCoupon.conditions.forEach(element => {
      if (element.field === 'Order total') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.subTotal.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.subTotal.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.subTotal.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      } else if (element.field === 'Order quantity') {
        if (element.operator === 'equal') {
          if (element.value[0] === this.totalItems.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'greater than') {
          if (element.value[0] <= this.totalItems.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        } else if (element.operator === 'less than') {
          if (element.value[0] >= this.totalItems.toString()) {
            this.checkFlexibleCouponByCustomer(flexiCoupon, coupon);
          } else {
            this.showInValidateCoupon();
          }
        }
      }
    });
  }
  checkFlexibleCouponByCustomer(flexiCoupon, coupon) {
    const userId = sessionStorage.getItem('userId');
    this.customerData = {
      customerId: userId,
      couponCode: coupon.couponCode
    };
    this.cartService.checkCouponOnCustomer(this.customerData).subscribe(data => {
      if (data.length === 0) {
        this.couponCalculation(flexiCoupon);
      } else {
        this.showInValidateCoupon();
      }
    }, error => {
      console.log(error);
    });
  }
  couponCalculation(selectedCoupon) {
    this.totalAmount = this.subTotal;
    if (selectedCoupon.amountType === 'Flat') {
      this.couponType = 'Flat';
      this.couponValue = selectedCoupon.typeValue;
      this.totalAmount = this.totalAmount - selectedCoupon.typeValue;
    } else if (selectedCoupon.amountType === 'Percentage') {
      this.couponType = 'Percentage';
      this.couponValue = selectedCoupon.typeValue;
      this.totalAmount = this.totalAmount - Math.round(this.totalAmount / 100 * selectedCoupon.typeValue);
    }
    if (!sessionStorage.getItem('coupon')) {
      this.showAppliedCoupon(selectedCoupon);
    }
  }
  showAppliedCoupon(selectedCoupon) {
    sessionStorage.setItem('coupon', selectedCoupon._id);
    this.message = 'Coupon Applied Successfully';
    this.matSnackBar.open(this.message, this.action, {
      duration: 1000,
      verticalPosition: 'top',
      panelClass: ['snackbar1']
    });
  }
  removeCoupon(subTotal) {
    this.total = subTotal;
    this.couponType = '';
    sessionStorage.removeItem('coupon');
  }
  checkCouponInExisting() {
    const couponId = sessionStorage.getItem('coupon');
    this.cartService.checkCouponForPlaceOrder(couponId).subscribe(data => {
      console.log(data);
      this.checkFirstTimeCoupon(data[0]);
    }, error => {
      console.log(error);
    });
  }
}
