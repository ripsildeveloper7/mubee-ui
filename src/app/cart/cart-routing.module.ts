
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import {PaymentSuccessComponent} from './checkout/payment-success/payment-success.component';

const routes: Routes = [
  {
    path: 'shopping',
    component: ShoppingCartComponent
  },
  {
    path: 'checkout',
    component: PlaceOrderComponent
  },
  {
    path: 'orderid/:id',
    component: PaymentSuccessComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class CartRoutingModule { }
