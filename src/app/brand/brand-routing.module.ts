import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrandNavbarComponent} from './shared/brand-navbar/brand-navbar.component';
import {BrandViewsComponent} from './brand-view/brand-views/brand-views.component';

const routes: Routes = [
  {
    path: 'brandNav' , component: BrandNavbarComponent
  },
  {
    path: 'brandview' , component: BrandViewsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandRoutingModule { }
