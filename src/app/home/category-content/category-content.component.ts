import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SuperCategory } from './category.model';
import {HomeService} from '../home.service';
import { AppSetting } from '../../config/appSetting';

@Component({
  selector: 'app-category-content',
  templateUrl: './category-content.component.html',
  styleUrls: ['./category-content.component.css']
})
export class CategoryContentComponent implements OnInit, AfterViewInit {
  showFiller = false;
  productImageUrl: string = AppSetting.categoryImageUrl;
  categoryModel: SuperCategory[];
  ProductHolder: any;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
  showMobileView = false;
  fourthFrame: any;
  fifthFrame: any;
constructor(private router: Router, private homeService: HomeService) {
  this.getCategoryBanner();
 }
getimage(id) {
this.router.navigate([]);
}
  ngOnInit()  {
    this.getCategoryDetails();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  // getCategoryDetails() {
  //  this.homeService.getCategory().subscribe(data => {
  //    console.log(data);
  //    this.ProductHolder = data.find(e => e.subCategory);
  //    this.firstFrame = data[0];
  //    console.log(this.firstFrame,"ff");
  //    this.secondFrame = data[1];
  //    this.thirdFrame = data[2];
  //    console.log(this.thirdFrame,"tt");
  //    console.log(this.ProductHolder,"pp");
  //  }, error => {
  //    console.log(error);
  //  });
  // }

  getCategoryDetails() {
    this.homeService.getCategory().subscribe(data => {
     console.log(data);
     const array: any = []; 
     const main = data[0].mainCategory;
     main.forEach(element => {
       element.subCategory.forEach(el => {
    array.push(el);
       })
    
     });
     console.log('hello', array);
     /* const sub = main[0].subCategory; */
 
      /* this.ProductHolder = data.find(e => e.subCategory); */
      this.firstFrame = array[0];
      console.log("first", this.firstFrame);
      this.secondFrame = array[1];
      this.thirdFrame = array[2];
      this.fourthFrame = array[3];
      this.fifthFrame = array[4];
      console.log(array, 'array');
    }, error => {
      console.log(error);
    });
   }
  getCategoryBanner() {
    this.homeService.getCategory().subscribe(data => {
      this.ProductHolder = data;
      console.log('category banner', data);
    }, error => {
      console.log(error);
    });
  }

}

