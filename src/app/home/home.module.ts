import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner/banner.component';
import { CategoryContentComponent } from './category-content/category-content.component';
import { ProductComponent } from './product/product.component';
import { Promotion1Component } from './promotion1/promotion1.component';
import { CarouselItemComponent } from './carousel-item/carousel-item.component';
import { Promotion2Component } from './promotion2/promotion2.component';
import { ReviewComponent } from './review/review.component';
import { StripComponent } from './strip/strip.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { CarouselItemDirective } from './carousel-item/carousel-item.directive';
import { AdsComponent } from './ads/ads.component';
import { Promotion3Component } from './promotion3/promotion3.component';
import { Promotion4Component } from './promotion4/promotion4.component';
import { Promotion5Component } from './promotion5/promotion5.component';
import { HoverDirective } from './promotion1/hover.directive';
import { SharedModule } from './../shared/shared.module';
import { SmallBannerComponent } from './small-banner/small-banner.component';
import { VideoComponent } from './video/video.component';
import { InstagramComponent } from './instagram/instagram.component';
import { EventBannerComponent } from './event-banner/event-banner.component';
import { BlogComponent } from './blog/blog.component';
import { ReferEarnComponent } from './refer-earn/refer-earn.component';
import { MatInputModule } from '@angular/material';
@NgModule({
  declarations: [BannerComponent,
    CategoryContentComponent, ProductComponent,
    Promotion1Component, Promotion2Component,
    ReviewComponent, StripComponent, SubscribeComponent,
    HomeComponent, CarouselItemComponent, CarouselItemDirective,
    AdsComponent, Promotion3Component, Promotion4Component, Promotion5Component, HoverDirective, SmallBannerComponent, VideoComponent, InstagramComponent, EventBannerComponent, BlogComponent, ReferEarnComponent],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
  ]
})
export class HomeModule { }
