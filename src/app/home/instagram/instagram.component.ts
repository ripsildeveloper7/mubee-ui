import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-instagram',
  templateUrl: './instagram.component.html',
  styleUrls: ['./instagram.component.css']
})
export class InstagramComponent implements OnInit {
  instaImages: any;
  constructor(public homeService: HomeService) { }

  ngOnInit() {
    this.getInstaApi();
  }
  getInstaApi() {
    this.homeService.getInstagramPics().subscribe(data => {
           this.instaImages = data;
           console.log('here --', this.instaImages);
    }, err => {
      console.log(err);
    });
  }
}
