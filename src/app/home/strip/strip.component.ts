import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from '../home.service';
import { AppSetting } from '../../config/appSetting';
@Component({
  selector: 'app-strip',
  templateUrl: './strip.component.html',
  styleUrls: ['./strip.component.css']
})
export class StripComponent implements OnInit {
  public name = 'krishnan';
  public newClass = 'balaji2';
  public oldClass = 'balaji3';
  public errorTime = true;
  public noerrorTime = false;
  public balaji = 'paraSin';
  public bala = 'hSix';
  showMobileView = false;
  instaImages: any;
  track = [{image: '../../../assets/image/1.png' , msg:'Genuine Products'},
{image: '../../../assets/image/free-delivery.png' , msg:'Free Delivery in India'},
{image: '../../../assets/image/free.png', msg:'Free Fall & Pico Services'},
{image: '../../../assets/image/logistics-delivery-truck-in-movement.png', msg:'Ready to Ship'}];

  final = [
    {
      finals: '../../../assets/img/Categories/5.jpg',
      kurtiName: 'BAGS',
      prize: '6 Products'
    },
    {
      finals: '../../../assets/img/Categories/16.jpg',
      kurtiName: 'BOOKING',
      prize: '6 Products'
    },
    {
      finals: '../../../assets/img/Categories/3.jpg',
      kurtiName: 'CLOTHING',
      prize: '12 Products'
    },
    {
      finals: '../../../assets/image/18.jpg',
      kurtiName: 'MEN',
      prize: '9 Products'
    },
    {
      finals: '../../../assets/image/19.jpg',
      kurtiName: 'NEW ONE',
      prize: '8 Products'
    },
    {
      finals: '../../../assets/image/6.jpeg',
      kurtiName: 'FASHION',
      prize: '8 Products'
    },
    {
      finals: '../../../assets/image/7.jpg',
      kurtiName: 'T.SHIRTS',
      prize: '12 Products'
    }

  ];

  constructor(public homeService: HomeService) { }

  ngOnInit() {
    this.getInstaPics();
  }
  getInstaPics() {
    this.homeService.getInstagramPics().subscribe(data => {
    this.instaImages = data;
    }, err => {
    console.log(err);
    });
  }
  ngAfterViewInit(){
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
}
