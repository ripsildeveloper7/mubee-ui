import { Component, OnInit, AfterViewInit } from '@angular/core';
import {HomeService} from '../home.service';
import { SharedService } from 'src/app/shared/shared.service';
import { MatSelect, MatSnackBar } from '@angular/material';
import { Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit, AfterViewInit {
  public name = 'krishnan';
  public newClass = 'balaji2';
  public oldClass = 'balaji3';
  public errorTime = true;
  public noerrorTime = false;
  public balaji = 'paraSin';
  public bala = 'hSix';
  validEmail = true;
  action;
  showMobileView = false;
  instaImages: any;
  message: string;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  btnColour =  '#6D6E71';
  emailError = true;
  constructor(public homeService: HomeService, private snackBar:MatSnackBar,private sharedService: SharedService) {
   
   }

  ngOnInit() {
    this.getInstaPics();

  }
  
  getInstaPics() {
    this.homeService.getInstagramPics().subscribe(data => {
    this.instaImages = data;
    }, err => {
    console.log(err);
    });
  }
  ngAfterViewInit(){
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  /* subscribe(val){
    this.sharedService.addSubscriber(val).subscribe(data =>{
    console.log(data);
    })
    } */
    subscribe(){
      var val = this.emailFormControl.value;
      this.message = "subscribed succesfully"
      this.sharedService.addSubscriber(val).subscribe(data =>{
        console.log(data);
        if(data.result){
          this.message = "Already subscribed"
          val = "";
        }
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
        })
    /*   let email = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
      if(val.match(email)){
        this.validEmail = true;
        this.message = "subscribed succesfully"
        this.sharedService.addSubscriber(val).subscribe(data =>{
          console.log(data);
          if(data.result){
            this.message = "Already subscribed"
            val = "";
          }
          this.snackBar.open(this.message, this.action, {
            duration: 500,
          });
          })
  
      } else {
        this.validEmail = false;
        console.log("Invalid");
      } */
      
      }



}
