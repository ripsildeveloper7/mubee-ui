import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  showPromotion5 = false;
  showPromotion4 = false;
  showPromotion3 = false;
  showPromotion2 = false;
  showPromotion1 = false;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.getPromotionFivethProduct();
    this.getPromotionFourthProduct();
    this.getPromotionThirdProduct();
    this.getPromotionSecondProduct();
    this.getPromotionFirstProduct();
  }
  getPromotionFivethProduct() {
    this.homeService.getSeventhRow().subscribe(data => {
      if (data.length === 0) {
        this.showPromotion5 = false;
      } else {
        this.showPromotion5 = true;
      }
    }, error => {
      console.log(error);
    });
  }
  getPromotionFourthProduct() {
    this.homeService.getSixthRow().subscribe(data => {
      if (data.length === 0) {
        this.showPromotion4 = false;
      } else {
        this.showPromotion4 = true;
      }
    }, error => {
      console.log(error);
    });
  }

  getPromotionThirdProduct() {
    this.homeService.getFifthRow().subscribe(data => {
      if (data.length === 0) {
        this.showPromotion3 = false;
      } else {
        this.showPromotion3 = true;
      }
    }, error => {
      console.log(error);
    });
  }
  getPromotionSecondProduct() {
    this.homeService.getThirdRow().subscribe(data => {
      if (data.length === 0) {
        this.showPromotion2 = false;
      } else {
        this.showPromotion2 = true;
      }
    }, error => {
      console.log(error);
    });
  }
  getPromotionFirstProduct() {
    this.homeService.getSecondRow().subscribe(data => {
      if (data.length === 0) {
        this.showPromotion1 = false;
      } else {
        this.showPromotion1 = true;
      }
    }, error => {
      console.log(error);
    });
  }
}
