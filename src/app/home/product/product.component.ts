import { Component, OnInit } from '@angular/core';
import { reduce } from 'rxjs/operators';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HomeService } from '../home.service';
import { AppSetting } from '../../config/appSetting';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  productModel: any;
  title: any;
  description: any;
  productImageUrl: string;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
  forthFrame: any;

  constructor(private homeService: HomeService) {
    this.productImageUrl = AppSetting.productImageUrl;
   }

  ngOnInit() {
    this.getThirdRow();
  }

  getThirdRow() {
    this.homeService.getThirdRow().subscribe(data => {
      this.title = data[0].title;
      this.description = data[0].description;
      this.productModel = data[0].productDetails;
      this.firstFrame = data[0].productDetails[0];
      this.secondFrame = data[0].productDetails[1];
      this.thirdFrame = data[0].productDetails[2];
      this.forthFrame = data[0].productDetails[3];
    }, error => {
      console.log(error);
    });
  }
/*   getProduct(id) {
    this.homeService.getProductPosition(id).subscribe(data => {
      this.productModel = data;
    }, error => {
      console.log(error);
    });
  }
  getSecondTag() {
    this.homeService.getSecondTag().subscribe(data => {
      this.title = data[0];
      this.description = data[0].tagValue[0];
      this.getProduct(data[0]._id);
    }, error => {
      console.log(error);
    });
  } */
  detailView(id) {

  }
}
