export class Banner {
    position: string;
    bannerImageName: string;
    bannerTitle: string;
    bannerImage: string;
    bannerDescription: string;
    link: string;
    bannerSubTitle: string;
    buttonContent: string;
}
