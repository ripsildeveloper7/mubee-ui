import { Component, OnInit, HostListener } from '@angular/core';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  isShow: boolean;
  topPosToStartShowing = 100;
  blogDetails : any;
  blogMedia : any;
  showMobileView = false;
  @HostListener('window:scroll')
  checkScroll() {


    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;



    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }
  // TODO: Cross browsing
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
  
  news = [
    {
    image: '../../../assets/img/News latter/n1.jpg' ,
    content: ' A Video Blog Post', comments: '1 COMMENT',
    subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed vulputate massa. Fusce ante magna, [...]'
  },
    {
    image: '../../../assets/img/News latter/n2.jpg' ,
    content: 'Just a cool blog post with Images', comments: '5 COMMENTS',
    subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed vulputate massa. Fusce ante magna, [...]'
  },
    {
    image: '../../../assets/img/News latter/n3.jpg' ,
    content: 'Another post with A Gallery', comments: '4 COMMENTS',
    subTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed vulputate massa. Fusce ante magna, [...]'
  }
];


  constructor(private homeService: HomeService) { 
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  ngOnInit() {
    this.getBlogDetails();
   
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  getBlogDetails() {
    this.homeService.getBlogDetails().subscribe(data => {
      this.blogDetails = data;

      console.log('this is blog details',this.blogDetails);
      this.getBlogMedia();
    }, err => {
      console.log(err);
    });
  }
  getBlogMedia() {
    this.homeService.getBlogMedia().subscribe(data => {
      this.blogMedia = data;
      console.log('this is blog media',this.blogMedia);
      this.blogArrayMap();
    }, err => {
      console.log(err);
    });
  }
//  const blog = this.blogDetails.reduce((byCode, rendered) => { byCode[rendered.Code] = rendered; return byCode; }, {} as { [key: string]: rendered });
  blogArrayMap() {
   this.blogDetails.forEach(e => {
     this.blogMedia.forEach(elemt => {
       if(e.featured_media === elemt.id) {
        e.imageUrl = elemt.guid.rendered;

       }
     });
   })
   console.log('updated', this.blogDetails);
  }

}
