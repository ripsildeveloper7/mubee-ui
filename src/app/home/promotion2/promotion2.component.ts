import { Component, OnInit, AfterViewInit } from '@angular/core';
import { reduce } from 'rxjs/operators';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HomeService } from '../home.service';
import { AppSetting } from '../../config/appSetting';
import { WishList } from '../../shared/model/wishList.model';
import { MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { Cart } from '../../shared/model/cart.model';
import { SigninDailogComponent } from 'src/app/shared/signin-dailog/signin-dailog.component';
@Component({
  selector: 'app-promotion2',
  templateUrl: './promotion2.component.html',
  styleUrls: ['./promotion2.component.css']
})
export class Promotion2Component implements OnInit, AfterViewInit {
  productModel: any;
  title: any;
  description: any;
  productImageUrl: string;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
  forthFrame: any;
  productValue: any;
  productLength: void;
  wishListLength;
  wishList: WishList[];
  wish: WishList;
  userId: string;
  showMobileView = false;
  productStore;
  count = 1;
  isLocalCart = false;
  message;
  isCart = false;
  action;
  addedCart;
  shopModel;
  cartModel;
  superCat;
  imageLoader = true;
  constructor(private dialog: MatDialog,private homeService: HomeService, private router: Router, private snackBar: MatSnackBar) {
    this.productImageUrl = AppSetting.productImageUrl;
   }

  ngOnInit() {
    this.getThirdRow();
    this.checkLogin();
    
    
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  onLoad(){
    this.imageLoader = false;
  }
  
  getThirdRow() {
    this.homeService.getThirdRow().subscribe(data => {
      this.productValue = data;
      console.log('3', this.productValue);
      this.productLength = data[0].productDetails.length;
      this.productModel = data[0].productDetails;
      this.getProduct();
    }, error => {
      console.log(error);
    });
  }
/*   getProduct(id) {
    this.homeService.getProductPosition(id).subscribe(data => {
      this.productModel = data;
    }, error => {
      console.log(error);
    });
  }
  getSecondTag() {
    this.homeService.getSecondTag().subscribe(data => {
      this.title = data[0];
      this.description = data[0].tagValue[0];
      this.getProduct(data[0]._id);
    }, error => {
      console.log(error);
    });
  } */
  checkLoginUser(product) {
    console.log('click', product);
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.addToWishList(product);
    } else {
      this.openDialog();
    }
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SigninDailogComponent, {
      panelClass: 'c1',

    });
  }
  addToWishList(product) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = product.productId;
    this.homeService.addToWishList(this.wish).subscribe(data => {
      this.wishList = data;
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }
  checkLogin() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getwishList();
    } else {

    }
  }
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.homeService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      this.wishListLength = data;
      sessionStorage.setItem('wishqty', this.wishListLength.length);
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }
  checkWishListEnable() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    sessionStorage.setItem('wislistLength', wishlist.length);
    const obj = {};
    for (const item of wishlist) {
      if (!obj[item.proId]) {
        const element = item.proId;
        obj[element] = true;
      }
    }
    for (const prod of this.productModel) {
      if (obj[prod.productId]) {
        prod.wishList = true;
      } else {
        prod.wishList = false;
      }
    }
  }
  /* getProduct() {
    this.homeService.getAllProduct().subscribe(data => {
      this.productStore = data;
      this.superCat = data.supercategoryId;

      console.log('all product', this.productStore);
    }, error => {
      console.log(error);
    });
  } */
  getProduct() {
    this.homeService.getAllProduct().subscribe(data => {
      this.productStore = data;
      console.log(this.productStore);
      this.productModel.forEach(e => {
         if (e.productId){
        e.productAll = this.productStore.find(item => item._id === e.productId);
      }})
      const productIds  = this.productModel.map(e => e.productId);
      console.log(productIds)
      this.productStore = this.productStore.filter(function(item){
        return productIds.indexOf(item._id) !== -1;
      });
      console.log(this.productStore);
      
    }, error => {
      console.log(error);
    });
  }
  getView(id, supId, subId) {
    if(subId){
      this.router.navigate(['product/viewsingle/', subId, id ]);
    } else {
      this.router.navigate(['product/viewsingle/', supId, id ]);
    }
  }
  addToBag(product) {
    this.productModel.forEach(element => {
      if (element.productId === product) {
        element.display = true;
      } else {
        element.display = false;
      }
    });
    console.log('afterProduct', this.productModel);
  }
  closeToBag(product) {
    this.productModel.forEach(element => {
      if (element.productId === product) {
        element.display = false;
      }
    });
  }
  skuProductAddToCart(productId, skuItem) {
    const userId = sessionStorage.getItem('userId');
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.getUserCart(userId, productId, skuItem);
      /* this.addToCartServer(userId, productId, skuItem); */
    } else {
      this.addToCartLocal(productId, skuItem);
      this.closeToBag(productId._id);
    }
  }
  addToCartLocal(product, skuItem) {
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      console.log('checkCArtLength', currentProduct);
      const item = {
        productId: product._id,
        sku: skuItem.sku,
        qty: this.count
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      console.log('chel2', cart);
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      console.log(totalItem);
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      const item = {
        productId: product._id,
        sku: skuItem.sku,
        qty: this.count
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.isLocalCart = false;
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.sku === element.items.sku)) {
          /* const localSame = cartLocal.find(s => s.items.sku === element.items.sku);
          localSame.items.qty += element.items.qty; */
          this.isLocalCart = true;
        }
        if (this.isLocalCart !== true) {
          cartLocal.push(element);
        }
      });
      if (this.isLocalCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    }
    }
  }
  getUserCart(userId, productId, skuItem) {
    this.homeService.shoppingUser(userId).subscribe(data => {
      this.isCart = false;
      this.addedCart = data;
      console.log(data);
      this.addedCart.forEach(element => {
        if (element.items.sku === skuItem.sku) {
          this.isCart = true;
        }
      });
      if (this.isCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
        this.addToCartServer(userId, productId, skuItem);
      }
      console.log(this.addedCart);
    }, error => {
      console.log(error);
    });

  }
  addToCartServer(userId, product, skuItem) {
    const totalItem: any = [];
    const cart = {
      productId: product._id,
      sku: skuItem.sku,
      qty: 1
    };
    totalItem.push(cart);
    this.cartModel = new Cart();
    this.cartModel.userId = userId;
    this.cartModel.items = totalItem;
    this.homeService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', this.shopModel.length);
      this.message = 'Product Added To Cart';
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
      this.closeToBag(product._id);
    }, error => {
      console.log(error);
    });
  }
  getViewAll() {
    const tempData = this.productModel.map(e => e.productId);
    this.router.navigate(['/product/supercategory/promotion/', this.productValue[0]._id], {queryParams: {'array': tempData, 'name': this.productValue[0].title, 'desc': this.productValue[0].description}});
  }
}
