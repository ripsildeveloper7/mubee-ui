import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {HomeService} from '../home.service';

@Component({
  selector: 'app-small-banner',
  templateUrl: './small-banner.component.html',
  styleUrls: ['./small-banner.component.css']
})
export class SmallBannerComponent implements OnInit, AfterViewInit {
  showFiller = false;
  categoryModel: any;
  holder: any;
  holderFrame1: any;
  holderFrame2: any;
  holderFrame3: any;
  showMobileView = false;
constructor(private router: Router, private homeService: HomeService) { }

  ngOnInit()  {
    this.getAds();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  getAds() {
    this.homeService.getAds().subscribe(data => {
      console.log('ads', data);
      this.holder = data;
      this.holderFrame1 = data[0];
      this.holderFrame2 = data[1];
      this.holderFrame3 = data[2];
    }, error => {
      console.log(error);
    });
  }
}

