export class  Promotion {
    /* _id: string; */
    promotionTitle: string;
    description: string;
    promotionPosition: number;
    productId: [{
        id: string;
    }];
}
