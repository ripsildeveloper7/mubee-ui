import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from '../home.service';
import { AppSetting } from '../../config/appSetting';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import { WishList } from '../../shared/model/wishList.model';
import { MatSnackBar } from '@angular/material';
import { MatDialog,MatDialogConfig } from '@angular/material';
import { Cart } from '../../shared/model/cart.model';
import { SigninDailogComponent } from 'src/app/shared/signin-dailog/signin-dailog.component';
import { WishlistService } from '../../wishlist/wishlist.service';
import { ProductService } from './../../product/product.service';
import { ELOOP } from 'constants';


@Component({
  selector: 'app-promotion1',
  templateUrl: './promotion1.component.html',
  styleUrls: ['./promotion1.component.css']
})
export class Promotion1Component implements OnInit, AfterViewInit {
  public names = 'flowers';
  public redClass = 'text-danger';
  public greenClass = 'text-success';

  title: any;
  description: any;
  productModel: any;
  productImageUrl: string;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
  forthFrame: any;
  productValue: any;
  productLength: void;
  wishListLength;
  wishList: WishList[];
  wish: WishList;
  userId: string;
  showMobileView = false;
  productStore;
  count = 1;
  isLocalCart = false;
  message;
  isCart = false;
  action;
  addedCart;
  variationType:any;
  shopModel;
  selectedItem;
  cartModel;
  totalSetCount:any;
  showPrice = false;
  constructor(private router: Router,private productService:ProductService, private homeService: HomeService, iconRegistry: MatIconRegistry,private dialog: MatDialog,
              sanitizer: DomSanitizer,private wishlistService: WishlistService, private snackBar: MatSnackBar) {
    this.productImageUrl = AppSetting.productImageUrl;
    iconRegistry.addSvgIcon(
      'hearts',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/hearts.svg'));
  }

  ngOnInit() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.showPrice = true;
    } else {
      this.showPrice = false;
    }
    this.getSecondRowProduct();
    // this.getLoginUser();
    this.getAllProductTag();
    
    this.getProduct();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  // getLoginUser(){
  //   const userId = sessionStorage.getItem('userId');
  //   if( userId === undefined || null ){
  //     this.showPrice =false;
  //   }else{
  //     this.showPrice = true;
  //   }
    
  // }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  submit() {
    this.router.navigate(['/home/subscribe']);
  }
  getSecondRowProduct() {
    this.homeService.getSecondRow().subscribe(data => {
      this.productValue = data;
      this.productLength = data[0].productDetails.length;
      this.productModel = data[0].productDetails;
      console.log('2 row',this.productModel);
     this.getProduct();
    }, error => {
      console.log(error);
    });
  }
  getProduct() {
    this.homeService.getAllProduct().subscribe(data => {
      this.productStore = data;
      console.log(this.productStore);
      this.productModel.forEach(e => {
         if (e.productId){
        e.productAll = this.productStore.find(item => item._id === e.productId);
          e.productAll.totalSetCount = 0;
      }})
      this.productModel.forEach(element => {
        element.productAll.child.forEach(el => {
          element.productAll.totalSetCount += el.ratio;
        });
      });
      console.log(this.productModel);
      const productIds  = this.productModel.map(e => e.productId);
      console.log(productIds);
     
      this.productStore = this.productStore.filter(function(item){
        return productIds.indexOf(item._id) !== -1;
      });
      console.log('value',this.productValue);
      this.checkLogin();
     console.log('2 row new',this.productStore);
    }, error => {
      console.log(error);
    });
   
  }
  getView(id, supId, subId) {
    if(subId){
      this.router.navigate(['product/viewsingle/', subId, id ]);
    } else {
      this.router.navigate(['product/viewsingle/', supId, id ]);
    }
  }
  // getView(id) {
  //   this.router.navigate(['product/viewsingle/', id]);
  // }
  getAllProductTag() {
    this.homeService.getAllProductTag().subscribe(data => {
    }, error => {
      console.log(error);
    });
  }

  checkLoginUser(product) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.addToWishList(product);
    } else {
      // this.router.navigate(['/account/acc/signin']);
      this.openDialog();
    }
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SigninDailogComponent, {
      // width: '750px',
      // height:'450px',
      panelClass:'c1',

    });
  }

  addToWishList(id) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = id;

    this.productService.addToWishList(this.wish).subscribe(data => {
      this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
      this.wishList.forEach(d => {
        if (d.productIds.proId == id) {
          this.productModel.wishList = true;
          this.message = 'Product Added To Wishlist';
          this.snackBar.open(this.message, this.action, {
            duration: 1000,
            verticalPosition: 'top',
              horizontalPosition: 'right',
            panelClass : ['snackbar']
          });
        } else {
          this.productModel.wishList = false;
          this.message = 'Product Removed from Wishlist';
          this.snackBar.open(this.message, this.action, {
            duration: 1000,
            verticalPosition: 'top',
              horizontalPosition: 'right',
            panelClass : ['snackbar']
          });
        }
      });
      this.checkLogin()
    }, err => {
      console.log(err);
    });
  }
  checkLogin() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getwishList();
    } else {

    }
  }
  removeWishList(wish) {
    this.wishlistService.deleteWishlist(this.userId, wish.productIds._id).subscribe(data => {
      this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
      }, err => {
        console.log(err);
      });
  }
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    console.log(this.userId);
    this.productService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      console.log('countWishList', data);
      this.wishListLength = data;
      sessionStorage.setItem('wishqty', this.wishListLength.length);
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }
  checkWishListEnable() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    sessionStorage.setItem('wislistLength', wishlist.length);
    const obj = {};
    for (const item of wishlist) {
      if (!obj[item.proId]) {
        const element = item.proId;
        obj[element] = true;
      }
    }
    for (const product of this.productValue) {
      for(const prod of product.productDetails){
        if (obj[prod.productAll._id]) {
          prod.productAll.wishList = true;
        } else {
          prod.productAll.wishList = false;
        }
      }
    
    }
    console.log('wishlist add',this.productValue);

  }


  addToBag(product) {
    this.productModel.forEach(element => {
      if (element.productId === product) {
        element.display = true;
      } else {
        element.display = false;
      }
    });
    console.log('afterProduct', this.productModel);
  }
  closeToBag(product) {
    this.productModel.forEach(element => {
      if (element.productId === product) {
        element.display = false;
      }
    });
  }
  skuProductAddToCart(product) {
    this.variationType = product.variationType;
    this.selectedItem = this.variationType === 'None' ? product.child.find(pro => pro.headChild === true) : this.selectedItem;

    const userId = sessionStorage.getItem('userId');
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.getUserCart(userId, product,  this.selectedItem);
      /* this.addToCartServer(userId, productId, skuItem); */
    } else {
      this.addToCartLocal(product,  this.selectedItem);
      this.closeToBag(product._id);
    }
  }
  addToCartLocal(product, skuItem) {
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      console.log('checkCArtLength', currentProduct);
      const item = {
        productId: product._id,
        INTsku: skuItem.INTsku,
        qty: 1
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      console.log('chel2', cart);
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      console.log(totalItem);
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      const item = {
        productId: product._id,
        INTsku: skuItem.INTsku,
        qty: 1
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.isLocalCart = false;
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.sku === element.items.sku)) {
          /* const localSame = cartLocal.find(s => s.items.sku === element.items.sku);
          localSame.items.qty += element.items.qty; */
          this.isLocalCart = true;
        }
        if (this.isLocalCart !== true) {
          cartLocal.push(element);
        }
      });
      if (this.isLocalCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    }
    }
  }
  getUserCart(userId, productId, skuItem) {
    this.homeService.shoppingUser(userId).subscribe(data => {
      this.isCart = false;
      this.addedCart = data;
      console.log(data);
      this.addedCart.forEach(element => {
        if (element.items.INTsku === skuItem.INTsku) {
          this.isCart = true;
        }
      });
      if (this.isCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
        this.addToCartServer(userId, productId, skuItem);
      }
      console.log(this.addedCart);
    }, error => {
      console.log(error);
    });

  }
  addToCartServer(userId, product, skuItem) {
    const totalItem: any = [];
    const cart = {
      productId: product._id,
      INTsku: skuItem.INTsku,
      qty: 1
    };
    totalItem.push(cart);
    this.cartModel = new Cart();
    this.cartModel.userId = userId;
    this.cartModel.items = totalItem;
    this.homeService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', this.shopModel.length);
      this.message = 'Product Added To Cart';
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
      this.closeToBag(product._id);
    }, error => {
      console.log(error);
    });
  }
  getViewAll() {
    const tempData = this.productModel.map(e => e.productId);
    this.router.navigate(['/product/supercategory/promotion/', this.productValue[0]._id], {queryParams: {'array': tempData, 'name': this.productValue[0].title, 'desc': this.productValue[0].description}});
  }
}
