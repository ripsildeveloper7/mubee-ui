import { Component, HostListener, OnInit } from '@angular/core';
import { HomeService } from '../home.service';
import { Router } from '@angular/router';
import { AppSetting } from 'src/app/config/appSetting';
import { SuperCategory } from '../category-content/category.model';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  showFiller = false;
  productImageUrl: string = AppSetting.mainCategoryBannerImageUrl;
  categoryModel: SuperCategory[];
  array:any;
  ProductHolder: any;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
 fourthFrame: any;
  fifthFrame: any;
  showMobileView = false;
constructor(private router: Router, private homeService: HomeService) {
  if (window.screen.width > 900) {
    this.showMobileView = false;
  } else {
    this.showMobileView = true;
  }
 }
getimage(id) {
this.router.navigate([]);
}
  ngOnInit()  {
    this.getCategoryDetails();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  getCategoryDetails() {
   this.homeService.getCategory().subscribe(data => {
    console.log(data);
    this.array = []; 
    const main = data;
    main.forEach(element => {
      element.mainCategory.forEach(el => {
   this.array.push(el);
  //  console.log(array,"main");
      })
   
    });
    console.log('hello', this.array);
    /* const sub = main[0].subCategory; */

     /* this.ProductHolder = data.find(e => e.subCategory); */
    //  this.firstFrame = this.array[0];
    
    //  this.secondFrame = this.array[1];
    //  this.thirdFrame = this.array[2];
    //  this.fourthFrame = this.array[3];
    //  this.fifthFrame = this.array[4];
     console.log(this.array, 'array');
   }, error => {
     console.log(error);
   });
   console.log("first", this.array);
  }



}
