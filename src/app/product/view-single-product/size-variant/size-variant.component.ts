import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Child } from '../../../shared/model/child.model';
import { PopUpSizeGuideService } from '../pop-up-size-guide/pop-up-size-guide.service';

@Component({
  selector: 'app-size-variant',
  templateUrl: './size-variant.component.html',
  styleUrls: ['./size-variant.component.css']
})
export class SizeVariantComponent implements OnInit, AfterViewInit {
  @Input() productModel: any;
  @Input() selectedSize: any;
  @Input() sizeData: any;
  @Input() isSizeGuide: boolean;
  @Output() selectedVariant = new EventEmitter<any>();
  selectedItem: Child;
  @ViewChild("foc",{static:true}) focus: ElementRef;
  constructor(private popService: PopUpSizeGuideService) { }

  ngOnInit() {
  }

  ngAfterViewInit(){

  }

  protected ngOnChanges(){
    if(this.selectedSize == true){
      this.focus.nativeElement.focus();
      this.focus.nativeElement.scrollIntoView({behavior:"smooth",block: "center"});
    }
  }
  sizeSelect(itemselect) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
   
  }
  getSizeChart(sizeChart) {
    this.popService.openPopUp(sizeChart);
  }
}
