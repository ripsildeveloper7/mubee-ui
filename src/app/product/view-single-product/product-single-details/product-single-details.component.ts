import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { Color } from './../../../shared/model/colorSetting.model';
import { Child } from './../../../shared/model/child.model';
import { SizeGuide } from '../../../shared/model/sizeGuide.model';
import { Attribute } from '../dynamic-attribute/attribute';
import { PopUpSizeGuideService } from '../pop-up-size-guide/pop-up-size-guide.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { InquiryFormComponent } from '../inquiry-form/inquiry-form.component';
import { Router } from '@angular/router';

declare const FB : any;
var twttr = twttr || {};
@Component({
  selector: 'app-product-single-details',
  templateUrl: './product-single-details.component.html',
  styleUrls: ['./product-single-details.component.css']
})
export class ProductSingleDetailsComponent implements OnInit {
  @Input() productModel: Product;
  @Input() color: Color;
  @Input() selectedSize: boolean;
  @Output() selectedVariant = new EventEmitter<any>();
  @Output() addTocart = new EventEmitter<any>();
  @Output() productImageSelected =  new EventEmitter<any>();
  @Input() productSizeGuide: SizeGuide;
  @Input() sizeData: any;
  @Input() selectedColor: string;
  @Input() attribute: Attribute;
  @Input() readyToWearholder: any;
  @Output() selectedService = new EventEmitter<any>();
  selectedItem: Child;
  showDescription = false;
  content: {
    selectedSize: string,
    serviceName: string,
    serviceId: string
  };
  char = 120;
  textData;
  file: any;
  url : any; 
  isReadyToWear = false;
  typeName: any;
  constructor(private router: Router,private dialog: MatDialog,private  popUpSizeGuideService: PopUpSizeGuideService) {
    this.url = "http://studentbus.in-ui.s3-website.ap-south-1.amazonaws.com/" + this.router.url;
    // console.log(this.url);
   }

  ngOnInit() {
    console.log(this.productModel,"model");
    (window as any).fbAsyncInit = function() {
   
      FB.init({
        appId: '464042414199162',
        cookie: false,  // enable cookies to allow the server to access
        // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.5
    });
  
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

  //   (function(d,s,id){
  //     var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
  //     if(!d.getElementById(id)){
  //         js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';
  //         fjs.parentNode.insertBefore(js,fjs);
  //     }
  // }(document, 'script', 'twitter-wjs'));
    }
  }

  


  fb(){
    console.log(this.router.url);
    FB.ui({
      method: 'feed',
      // href: urlHost+link,
      // method: 'feed',
      title: this.productModel.productName,
      link: "www.ucchalfashion.com/" + this.router.url,
      picture: this.productModel.productImage[0].productImageName,
      // caption: "Interbank",
      description:this.productModel.productName
    });
  }

  pinIt(){
    window.open("//www.pinterest.com/pin/create/button/"+
    "?url="+this.url+
    "&media="+this.productModel.productImage[0].productImageName+
    "&description="+this.productModel.productName,"_blank");
    return false;
  }

  
  tweet(){
    return {
      link: function(scope, element, attr) {
          setTimeout(function() {
                  twttr.widgets.createShareButton(
                      this.url,
                      element[0],
                      // function(el) {}, {
                      //     count: 'none',
                      //     text: productModel
                      // }
                  );
          });
      }
  }
}
  
  
  sizeSelect(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  /*   this.selectedItem.selectSize = itemselect.sizeName; */
  }
  addToCartItem(proId) {
    /* if(this.selectedColor){
      const prod = this.productModel.child.find(el => el.colorId === this.selectedColor);
      this.selectColorSize(prod);
    } */
    this.addTocart.emit(proId);
  }
  selectColorSize(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  }
  selectColor(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  }
  /* getDescription() {
    if (this.showDescription === false) {
      console.log(this.productModel);
      const file = this.productModel.productDescription;
      this.textData = file.substr(0, this.char);
      console.log(this.textData);
    } else {
      this.textData = this.productModel.productDescription;
    }
  } 
  /* checkLoginUser(product) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.WishList.emit(product);
    } else {
      this.router.navigate(['/account/acc/signin']);
    }
  } */
  selectImageModel(event) {
    /* console.log(event); */
    this.productImageSelected.emit(event);
  }
  getPopUp() {
    this.popUpSizeGuideService.openPopUp(this.productSizeGuide);
    
  }
  readMore() {
    this.showDescription = true;
  }
  readLess() {
    this.showDescription = false;
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(InquiryFormComponent, {
      panelClass: 'c1',

    });
  }
  changeApplyOn(name) {
    this.typeName = name.value;
    console.log(name.value);
    this.isReadyToWear = true;
  }
  getSizeChart(sizeChart) {
    this.popUpSizeGuideService.openPopUp(sizeChart);
  }
  selectedSizeValue(element, readyTo) {
    /* console.log(element.value, readyTo); */
    this.content = {
      selectedSize: element.value,
      serviceName: readyTo[0].serviceName,
      serviceId: readyTo[0]._id
    };
    this.selectedService.emit(this.content);
  }
}
