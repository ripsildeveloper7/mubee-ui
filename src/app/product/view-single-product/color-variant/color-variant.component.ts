import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Child } from '../../../shared/model/child.model';
import { Color } from '../../../shared/model/colorSetting.model';

@Component({
  selector: 'app-color-variant',
  templateUrl: './color-variant.component.html',
  styleUrls: ['./color-variant.component.css']
})
export class ColorVariantComponent implements OnInit {
  @Input() productModel: any;
  @Input() selectedSize: any;
  @Input() sizeColor: Color;
  @Input() selectedColorMain: string;
  @Output() selectedVariantColor = new EventEmitter<any>();
  @Output() selectedImage = new EventEmitter<any>();
  @ViewChild("foc",{static:false}) focus: ElementRef;
  selectedItem: Child;
  constructor() { }

  ngOnInit() {
    console.log(this.productModel.child)
  }
  protected ngOnChanges(){
    if(this.selectedSize == true){
      this.focus.nativeElement.focus();
      this.focus.nativeElement.scrollIntoView({behavior:"smooth",block: "center"});
    }
  }
  /* colorSelect(itemselect) {
    this.selectedItem = itemselect;
    this.selectedVariantColor.emit(this.selectedItem);
  } */
  selectedColor(colorProduct) {
    this.selectedVariantColor.emit(colorProduct);
  }
}
