import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { Child } from './../../../shared/model/child.model';
import { Cart } from './../../../shared/model/cart.model';
import { Color } from './../../../shared/model/colorSetting.model';
import { ProductService } from './../../../product/product.service';
import { MatSnackBar } from '@angular/material';
import { AppSetting } from './../../../config/appSetting';
import {
  ActivatedRoute, Router, NavigationEnd, NavigationStart,
  PRIMARY_OUTLET, RoutesRecognized, Params, ParamMap, Data, Event
} from '@angular/router';
import { Zoom } from './../product-zoom/zoom.model';
import { WindowScrollingService } from './../product-zoom/window-scrolling.service';
import { WishList } from './../../../shared/model/wishList.model';
import { Attribute } from './../dynamic-attribute/attribute';
import { AttributeService } from './../dynamic-attribute/attribute.service';
import { ProductAttributeComponent } from './../dynamic-attribute/product-attribute/product-attribute.component';
import { RecentProduct } from '../../../shared/model/recentProduct.model';
import { Subscription } from 'rxjs';
import { SharedService } from '../../../shared/shared.service';
import { TestBed } from '@angular/core/testing';
@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit, AfterViewInit {
  productModel: any;
  zoom: Zoom;
  wishList: WishList[];
  wish: WishList;
  productImageUrl: string = AppSetting.productImageUrl;
  productModelImage = [];
  id: string;
  sildeIndex = 0;
  color: Color;
  showRelatedProducts;
  productId;
  relatedProducts = [];
  primeHide: boolean;
  showImages: boolean;
  selectedSmallImg: any;
  selectedImg;
  cartModel: Cart;
  shopModel: any = [];
  message;
  noPrductAdd = false;
  selectedItem: Child;
  selectedSize: boolean;
  count = 1;
  reviewData: any;
  totalRating: number;
  productSizeGuide: any;
  product: Product;
  text = 'block';
  action: string;
  discountStore: any;
  tempDiscount: any;
  userId: string;
  variationType: string;
  selectedColor: string;
  attribute: Attribute[];
  attributeLength: number;
  addedCart: any;
  isCart = false;
  isLocalCart = false;
  isRecentProduct = false;
  selectProduct;
  recentPrdouctData;
  brandProductData;
  showRecent = false;
  isSizeGuide = false;
  showLike = false;
  breadCrumbDetails;
  categorySelected: Array<any>;
  selectedSizeCategory: Array<any>;
  selectedColorCategory: Array<any>;
  sizeColorVariantColors: Array<any> = [];
  RecentProduct: { productId: [string] };
  sizeData: any;
  showMobileView1 = false;
  alsoLike: {
    sp: number,
    catalogueName: string,
    fabric: string
  };
  alsoModel: any;
  attributeEnableData:  Array<any> = new Array();
  readyToWearData: any;
  readyToWearholder: any = [];
  selectedService: any;
  constructor(private router: Router, private route: ActivatedRoute, private productService: ProductService,
              private sharedService: SharedService, private snackBar: MatSnackBar,
              private windowScrollingService: WindowScrollingService, private attributeService: AttributeService) {

  }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      console.log('product details', data.product);
      this.breadCrumbDetails = data.product;
      this.product = data.product;
      this.categorySelected = data.category;
      const filterAttributeData = this.categorySelected.filter(enable=> (enable.fieldSetting !== 'Color') && (enable.fieldSetting !== 'Size'));
      
      this.attributeEnableData = filterAttributeData.filter(enable => enable.fieldEnable === true).map(el => el._id);
      console.log(this.attributeEnableData);
      this.variationType = this.product.variationType;
      this.checkVariation(this.variationType);
      this.discountCalculation();
      this.getSizeGuide();
      /* this.getDiscount();
      this.getProductReview();
      this.checkLogin(); */
      this.checkLogin();
      this.zoom = new Zoom();
      this.zoom.displayClass = 'displayNone';
    });
  }

  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView1 = false;
    } else {
      this.showMobileView1 = true;
    }
  }
  getSizeGuide() {
    if (this.product.subCategoryId) {
      this.productService.getSizeGuideSubCategory(this.product.subCategoryId).subscribe(data => {
        console.log('size', data);
        this.sizeData = data;
        this.isSizeGuide = true;
      }, error => {
        console.log(error);
      });
    } else if (this.product.superCategoryId) {
      this.productService.getSizeGuideSuperCategory(this.product.superCategoryId).subscribe(data => {
        console.log('size', data);
        this.sizeData = data;
        this.isSizeGuide = true;
      }, error => {
        console.log(error);
      });
    }
    /* if (this.sizeData.length !== 0) {
      this.isSizeGuide = true;
    } */
  }
  discountCalculation() {
    const discount = 100 - this.productModel.discount;
    const totalPrice = this.productModel.sp * (100 / discount);
    const savePrice = totalPrice - this.productModel.sp;
    this.productModel.savePrice = savePrice;
    this.productModel.totalPrice = totalPrice;
    /* this.productModel.readyToWear = true; */
   /*  this.checkReadyToWear(); */
    this.getReadyToWear();
    console.log('discount check', this.productModel);
   /*  for (let i = 0; i <= this.productModel.length - 1; i++) {
      this.productModel[i].savePrice = this.productModel[i].sp * (this.productModel[i].discount / 100);
      this.productModel[i].totalPrice = this.productModel[i].sp + this.productModel[i].savePrice;
    } */
    this.getAlsolike();
  }
  /* checkReadyToWear() {
    if (this.productModel.readyToWear === true) {
      this.readyToWearData.forEach(element => {
        if (element.selectedCategoryID === this.productModel.superCategoryId) {
          this.readyToWearholder = element;
        }
      });
    }
  } */
  firstImageSlider() {
    this.productModel.productImage.forEach((element, index) => {
      if (index === 0) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  }
  checkVariation(variation) {
    switch (variation) {
      case 'None': {
        /* this.productModel = this.product; */
        this.parentContentChange();
        break;
      }
      case 'Color': {
        this.color = new Color();
        this.color.ids = this.product.child.map(color => color.colorId);
        this.categorySelected.filter(select => select.fieldSetting === 'Color');
        /* this.viewAllColors(this.color) */
        this.viewSelectedColors();
        break;
      }
      case 'SizeColor': {
        this.color = new Color();
        const a = this.categorySelected.reduce((selectSize, item) => {
          if (item.fieldSetting === 'Size') { selectSize = item; }
          return selectSize;
        }, null);

        const b = this.categorySelected.reduce((selectColor, item) => {
          if (item.fieldSetting === 'Color') { selectColor = item; }
          return selectColor;
        }, null);

        this.color.ids = this.product.child.map(color => color.colorId);
        this.color.ids.forEach(che => {
          if (this.sizeColorVariantColors.indexOf(che) === -1) {
            this.sizeColorVariantColors.push(che);
          }
        });
        this.selectedSizeCategory = a.fieldValue;
        this.selectedColorCategory = b.fieldValue;
        console.log(this.sizeColorVariantColors);
        console.log(this.selectedColorCategory);
        const intersections = this.selectedColorCategory.filter(e => this.sizeColorVariantColors.indexOf(e._id) !== -1);
        console.log(intersections);
        this.parentContentChange();
        /* console.log(this.sizeColorVariantColors); */


        /* this.product.child.map(size => size.sizeVariantId);
        console.log(this.product); */

        /* this.color.ids = this.product.child.map(color => color.colorId);
        this.color.ids = this.product.child.map(color => color.sizeVariantId); */
        /* this.viewAllColors(this.color) */
        break;
      }
      case 'Size': {
        this.parentContentChange();
        break;
      }
      default: {
        // statements;
        break;
      }
    }
  }
  viewSelectedColors() {
    const headChildId = this.product.child.find(el => el.headChild === true);
    this.productModel = this.product;
    this.discountCalculation();
    this.productModel.child.forEach(element => {
      element.headChild = false;
      if (element.colorId === headChildId.colorId) {
        element.headChild = true;
        this.productModel.productName = element.productName;
        this.productModel.productDescription = element.productDescription;
        this.productModel.price = element.price;
        this.productModel.INTsku = element.INTsku;
        this.productModel.productImage = element.productImage;
        console.log('attribute', element.attribute);
        element.attribute = element.attribute.filter(item => {
          return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        });
        const keysValue = this.getKeys(element.attribute)
        console.log(keysValue,"keys");
        this.productModel.attribute = this.attributeService.getAds(keysValue);
        this.attribute = this.productModel.attribute;

        this.productModel.costIncludes = this.productModel.costIncludes !== undefined ? this.productModel.costIncludes.split('.') : '';
        this.firstImageSlider();
      }
    });
  }

  getProductSizeGuide(id) {
    this.productService.getSelectedProductSizeGuide(id).subscribe(data => {
      this.productSizeGuide = data;
    }, error => {
      console.log(error);
    });
  }

  public getKeys = (arr) => {
    var key, keys = [];
    for (let i = 0; i < arr.length; i++) {
      for (key in arr[i]) {
        if (arr[i].hasOwnProperty(key)) {
          keys.push({ name: key, value: arr[i][key] });
        }
      }
    }
    this.attributeLength = keys.length;
    console.log('attribute length', this.attributeLength);
    return keys.filter(na => (na.name !== '_id') && (na.name !== 'attributeId') &&(na.name !== 'attributeFieldId') );
}
  clickSelectedImgeZoom(selectIndex) {
    this.productModel.productImage.forEach((element, index) => {
      if (index === selectIndex) {
        this.sildeIndex = index;
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
    this.zoom = new Zoom();
    this.zoom.displayClass = 'displayBlock';
    this.zoom.imageData = this.productModel.productImage;
    this.windowScrollingService.disable();
  }
  selectData(event) {
    this.selectedItem = event;
    this.selectedContentChange(event);
  }
  parentContentChange() {
    /* this.productModel.productImage = event.productImage; */
    this.productModel = this.product;
    console.log('p', this.productModel);
    this.productModel.child.forEach((element) => {
      if (element.headChild === true) {
        this.productModel.productName = element.productName;
        this.productModel.productDescription = element.productDescription;
        this.productModel.price = element.price;
        this.productModel.INTsku = element.INTsku;
        this.productModel.productImage = element.productImage;
        console.log('parentcontetattribute', element.attribute);
        element.attribute = element.attribute.filter(item => {
          return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        });
        const keysValue = this.getKeys(element.attribute);
        console.log(keysValue,"keysV");
        this.productModel.attribute = this.attributeService.getAds(keysValue);
        this.productModel.costIncludes  =  element.costIncludes !== undefined ? element.costIncludes.split('.') : [];
        console.log(this.productModel.costIncludes)
        this.attribute = this.productModel.attribute;
      }
    });
    this.firstImageSlider();
  }
  selectedContentChange(selected) {
    this.productModel.child.forEach((element) => {
      element.headChild = false;
      if (element.INTsku === selected.INTsku) {
        element.headChild = true;
        this.productModel.productName = element.productName;
        this.productModel.productDescription = element.productDescription;
        this.productModel.price = element.price;
        this.productModel.INTsku = element.INTsku;
        this.productModel.productImage = element.productImage;
        // this.productModel.careInstructions = element.careInstructions;
        element.attribute = element.attribute.filter(item => {
          return this.attributeEnableData.indexOf(item.attributeId) !== -1;
        });
        const keysValue = this.getKeys(element.attribute);
        this.productModel.attribute = this.attributeService.getAds(keysValue);
        this.productModel.costIncludes = element.costIncludes !== undefined ? element.costIncludes.split('.') : [];
        this.attribute = this.productModel.attribute;
      }
    });
    this.firstImageSlider();
  }
  selectedItems(productId) {
    this.selectedItem = this.variationType === 'None' ? this.productModel.child.find(pro => pro.headChild === true) : this.selectedItem;
    if (!this.selectedItem) {
      this.selectedSize = true;
    } else {
      this.selectedSize = false;
      this.skuProductAddToCart(productId, this.selectedItem);
      console.log(this.selectedItem,"selected");
    }
  }
  skuProductAddToCart(productId, skuItem) {
    const userId = sessionStorage.getItem('userId');
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.addToCartServer(userId, productId, skuItem);
    } else {
      this.addLocalCart(productId, skuItem);
    }
  }
  addLocalCart(productId, skuItem) {
 
      this.addToCartLocal(productId, skuItem);
    
  }
  addToCartLocalOnReadyToWear(product, skuItem) {
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        tailoringService: true,
        selectedSize: this.selectedService.selectedSize,
        serviceId: this.selectedService.serviceId
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        tailoringService: true,
        selectedSize: this.selectedService.selectedSize,
        serviceId: this.selectedService.serviceId
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.INTsku === element.items.INTsku) ) {
          if (cartLocal.find(s => s.items.tailoringService === true && element.items.tailoringService === true)) {
            if (cartLocal.find(s => s.items.serviceId === element.items.serviceId)) {
              if (cartLocal.find(s => s.items.selectedSize === element.items.selectedSize)) {
                const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku);
                localSame.items.qty += element.items.qty;
              } else {
                cartLocal.push(element);
              }
            } else {
              cartLocal.push(element);
            }
        } else if (cartLocal.find(s => s.items.tailoringService !== true && element.items.tailoringService === true)) {
          cartLocal.push(element);
        } else if (cartLocal.find(s => s.items.tailoringService === false && element.items.tailoringService === false)) {
          const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku && element.items.tailoringService === false);
          localSame.items.qty += element.items.qty;
        } else if (cartLocal.find(s => s.items.tailoringService !== false && element.items.tailoringService === false)) {
          cartLocal.push(element);
        }
        } else {
          cartLocal.push(element);
        }
      });
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
    }
    this.router.navigate(['/cart/shopping']);
  }
  addToCartLocal(product, skuItem) {
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        tailoringService: false
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(this.productModel);
      const item = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        tailoringService: false
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.INTsku === element.items.INTsku && s.items.tailoringService === element.items.tailoringService)) {
          const localSame = cartLocal.find(s => s.items.INTsku === element.items.INTsku && s.items.tailoringService === element.items.tailoringService);
          localSame.items.qty += element.items.qty;
        } else {
          cartLocal.push(element);
        }
      });
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 2000,
      });
    }
    // this.router.navigate(['/cart/shopping']);
  }
  addToCartServer(userId, product, skuItem) {
    console.log(skuItem.INTsku,"sku");
    if (this.selectedService !== undefined) {
      const totalItem: any = [];
      const cart = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
        
        
      };
      totalItem.push(cart);
      this.cartModel = new Cart();
      this.cartModel.userId = userId;
      this.cartModel.items = totalItem;
      /* this.cartModel.tailoringService = this.selectedService; */
      this.productService.addToCart(this.cartModel).subscribe(data => {
        this.shopModel = data;
        sessionStorage.setItem('cartqty', this.shopModel.length);
        this.message = 'Product Added To Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 3000,
        });
      }, error => {
        console.log(error);
      });
    } else {
      const totalItem: any = [];
      const cart = {
        productId: product,
        INTsku: skuItem.INTsku,
        qty: this.count,
   
      };
      
      totalItem.push(cart);
      this.cartModel = new Cart();
      this.cartModel.userId = userId;
      this.cartModel.items = totalItem;
      /* this.cartModel.tailoringService = this.selectedService; */
      this.productService.addToCart(this.cartModel).subscribe(data => {
        this.shopModel = data;
        sessionStorage.setItem('cartqty', this.shopModel.length);
        this.message = 'Product Added To Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 3000,
        });
      }, error => {
        console.log(error);
      });
    }

    // this.router.navigate(['/cart/shopping']);
  }
  actionPlus(plus) {
    this.count = ++plus;
  }
  actionMinus(minus) {
    this.count = --minus;
  }
  getProductReview() {
    this.productService.getSingleProductReview(this.id).subscribe(data => {
      this.reviewData = data;
      let sum = 0;
      for (let i = 0; i <= data.length - 1; i++) {
        sum += data[i].rating;
      }
      this.totalRating = Math.round(sum / data.length);
    }, error => {
      console.log(error);
    });
  }
  getDiscount() {
    this.productService.getAllDiscount().subscribe(data => {
      this.discountStore = data;
      this.productModel.discountStyle = 'discountNone';    // set all discountStyle field 'discountNone'
      for (let i = 0; i <= this.discountStore.length - 1; i++) {
        for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
          for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {


            if (this.discountStore[i].conditions[j].field === 'Product Name') {  // check discount field

              if (this.productModel._id === this.discountStore[i].conditions[j].value[k]) {

                if (this.productModel.discountStyle === 'discountStyle') {   // check offer already applied or not
                  if (this.discountStore[i].amountType === 'Flat') {          // check amount type
                    const temp = this.productModel.oldPrice - this.discountStore[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {    // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                      this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                      console.log(this.productModel.price);
                    } else {      // new price is smallerthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {          // amount type percentage
                    const temp = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));


                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {   // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                      this.productModel.discount = this.discountStore[i].typeValue + '%';
                      console.log(this.productModel.price);
                    } else {     // new price is smallerthan previous price
                      continue;
                    }
                  }

                  /* check amount greaterthan or lessthan ----- end ---------*/

                  /* previous offer not applied product-------------- start ------------- */


                } else {
                  this.productModel.discountStyle = 'discountStyle';
                  this.productModel.oldPrice = this.productModel.price;
                  if (this.discountStore[i].amountType === 'Flat') {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                    this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                    console.log(this.productModel.price);
                  } else {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                    this.productModel.discount = this.discountStore[i].typeValue + '%';
                    console.log(this.productModel.price);
                  }
                }

                /* previous offer not applied product-------------- end ------------- */

              }
            } else if (this.discountStore[i].conditions[j].field === 'Product Category') {  // check discount field
              if (this.productModel.superCategoryId === this.discountStore[i].conditions[j].value[k]) {
                if (this.productModel.discountStyle === 'discountStyle') {  // check offer already applied or not
                  if (this.discountStore[i].amountType === 'Flat') {     // check amount type
                    const temp = this.productModel.oldPrice - this.discountStore[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {     // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                      this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                      console.log(this.productModel.price);
                    } else {     // new price is lesserthan previous price
                      continue;
                    }
                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {     // amount type percentage
                    const temp = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel.price > temp) {  // new price is lesserthan previous price
                      this.tempDiscount = this.discountStore[i];
                      this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                      this.productModel.discount = this.discountStore[i].typeValue + '%';
                      console.log(this.productModel.price);
                    } else {
                      continue;
                    }
                  }
                } else {
                  this.productModel.discountStyle = 'discountStyle';
                  this.productModel.oldPrice = this.productModel.price;
                  if (this.discountStore[i].amountType === 'Flat') {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - this.discountStore[i].typeValue;
                    this.productModel.discount = 'Flat ' + this.discountStore[i].typeValue;
                    console.log(this.productModel.price);
                  } else {
                    this.tempDiscount = this.discountStore[i];
                    this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.discountStore[i].typeValue));
                    this.productModel.discount = this.discountStore[i].typeValue + '%';
                    console.log(this.productModel.price);
                  }
                }
              }
            }

          }
        }
      }
    }, error => {
      console.log(error);
    });
  }
  /* selectSizeData(event) {
    this.selectedItem = event;
    if (this.productModel.discount !== 0 && this.productModel.discount !== undefined) {
      this.productModel.discountStyle = 'discountStyle';
      this.productModel.oldPrice = event.price;
      if (this.tempDiscount.amountType === 'Flat') {
        this.productModel.price = this.productModel.oldPrice - this.tempDiscount.typeValue;
      } else {
        this.productModel.price = this.productModel.oldPrice - Math.round((this.productModel.oldPrice / 100 * this.tempDiscount.typeValue));
      }
      this.selectedItem = event;
    } else {
      this.productModel.price = event.price;
      this.productModel.discountStyle = 'discountNone';
      this.selectedItem = event;
    }
  } */

  /*  checkLogin() {
     if (JSON.parse(sessionStorage.getItem('login'))) {
       this.userId = sessionStorage.getItem('userId');
       this.getwishList();
     } else {
     }
   } */
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.productService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }

  checkWishListEnable() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    sessionStorage.setItem('wislistLength', wishlist.length);
    for (const item of wishlist) {
      if (item.proId === this.productModel._id) {
        this.productModel.wishList = true;
      }
    }

  
  }
  addRecentProductLocalStorage(product) {
    const storedValue = JSON.parse(localStorage.getItem('recent')) || [];
    console.log(storedValue);
    const array: any = [];
    let isStored = false;
    array.push(product._id);
    if (storedValue.length === 0) {
      localStorage.setItem('recent', JSON.stringify(array));
    } else {
      storedValue.forEach(element => {
        if (element === product._id) {
          isStored = true;
        } else {
          isStored = false;
          array.push(element);
        }
      });
      if (isStored === false) {
        localStorage.setItem('recent', JSON.stringify(array));
      }
    }
    this.getRecentProductfromLocalStorage();
  }
  addRecentProductServer(userId, product, recentProduct) {
    if (recentProduct.length === 0) {
      this.addDirectRecentProduct(userId, product);
      this.showRecent = true;
    } else {
      this.isRecentProduct = false;
      recentProduct[0].productId.forEach(element => {
        if (element === product._id) {
          this.isRecentProduct = true;
        }
      });
      if (!this.isRecentProduct) {
        this.addDirectRecentProduct(userId, product);
      }
    }
  }
  /* addUploadRecentProduct(userId, product) {
    const temp = new RecentProduct();
    temp.userId = userId;
    temp.productId = product._id;

  } */
  addDirectRecentProduct(userId, product) {
    const temp = new RecentProduct();
    temp.userId = userId;
    temp.productId = product._id;
    this.productService.addRecentProductID(temp).subscribe(data => {
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  getRecentProduct(userId, product) {
    this.productService.getRecentProductID(userId).subscribe(data => {
      console.log('recent', data);
      this.addRecentProductServer(userId, product, data);
    }, error => {
      console.log(error);
    });
  }
  checkLogin() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getwishList();
      this.getRecentProduct(this.userId, this.productModel);
      this.viewRecentProduct(this.userId);
    } else {
      this.addRecentProductLocalStorage(this.productModel);
      
    }
  }
  getRecentProductfromLocalStorage() {
    const recentId = JSON.parse(localStorage.getItem('recent'));
    this.RecentProduct = {
      productId: recentId
    };
    this.productService.getRecentProductByLocalStorage(this.RecentProduct).subscribe(data => {
      this.recentPrdouctData = data;
      this.recentPrdouctData = this.recentPrdouctData.filter(element => element._id !== this.id);
      // this.sharedService.multipleProductDiscount(this.discountStore, this.recentPrdouctData);
      console.log('reent', this.recentPrdouctData);
      if (this.recentPrdouctData.length !== 0) {
        this.showRecent = false;
      } else {
        this.showRecent = true;
      }
    }, error => {
      console.log(error);
    });
  }
  viewRecentProduct(userId) {
    this.productService.viewRecentProduct(userId).subscribe(data => {
      this.recentPrdouctData = data;
      this.recentPrdouctData = this.recentPrdouctData.filter(element => element._id !== this.id);
      this.sharedService.multipleProductDiscount(this.discountStore, this.recentPrdouctData);
      this.checkTotalWishlist();
    }, error => {
      console.log(error);
    });
  }
  checkTotalWishlist() {
    const wish = new WishList();
    wish.userId = this.userId;
    this.productService.getWishList(wish).subscribe(data => {
      this.wishList = data;
      this.checkWishListEnable();
      this.checkWishListEnableSimilarProduct();
    }, error => {
      console.log(error);
    });
  }
  checkWishListEnableSimilarProduct() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    const obj = {};
    for (const item of wishlist) {
      if (!obj[item.proId]) {
        const element = item.proId;
        obj[element] = true;
      }
    }
    for (const prod of this.recentPrdouctData) {
      if (obj[prod._id]) {
        prod.wishList = true;
      } else {
        prod.wishList = false;
      }
    }
    for (const prod of this.brandProductData) {
      if (obj[prod._id]) {
        prod.wishList = true;
      } else {
        prod.wishList = false;
      }
    }
  }
  getBrandedProduct(brandId) {
    this.productService.getAllBrandProduct(brandId).subscribe(data => {
      this.brandProductData = data;
      this.brandProductData = this.brandProductData.filter(e => e._id !== this.id);
      this.sharedService.multipleProductDiscount(this.discountStore, this.brandProductData);
    }, error => {
      console.log(error);
    });
  }
  getAlsolike() {
    this.alsoLike = {
      sp: this.productModel.sp,
      catalogueName: this.productModel.catalogueName,
      fabric: this.productModel.fabric
    };
    this.productService.getAlsoLike(this.alsoLike).subscribe(data => {
      this.alsoModel = data;
      this.alsoModel = this.alsoModel.filter(element => element._id !== this.productModel._id);
    }, error => {
      console.log(error);
    });
  }
  getReadyToWear() {
    this.productService.getAllReadyToWear().subscribe(data => {
      console.log('ready to wear', data);
     this.productModel.tailoringService = 'Yes'; 
      this.readyToWearData = data;
      if (this.readyToWearData.length !== 0) {
        this.readyToWearData.forEach(element => {
          if (element.selectedCategoryID === this.productModel.superCategoryId) {
            this.readyToWearholder.push(element);
          }
        });
      }
      console.log('after readyto', this.readyToWearholder);
    }, error => {
      console.log(error);
    });
  }
  getSelectedService(element) {
    this.selectedService = element;
    console.log(this.selectedService);
  }
}
