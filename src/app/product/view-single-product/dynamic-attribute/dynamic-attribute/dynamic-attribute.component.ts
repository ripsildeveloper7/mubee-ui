import { Component, Input, OnInit, ViewChild, ComponentFactoryResolver, OnDestroy } from '@angular/core';

import { AttributeDirective } from './../attribute.directive';
import { AdComponent } from './../attribute.component';
import { Attribute } from './../attribute';
@Component({
  selector: 'app-dynamic-attribute',
  templateUrl: './dynamic-attribute.component.html',
  styleUrls: ['./dynamic-attribute.component.css']
})
export class DynamicAttributeComponent implements OnInit {
  @Input() attribute: Attribute[];
  currentAdIndex = 0;
  @ViewChild(AttributeDirective, {static: true}) adHost: AttributeDirective;
  interval: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.loadComponent();
  }


  loadComponent() {
    this.currentAdIndex = (this.currentAdIndex) % this.attribute.length;
    /* console.log(this.currentAdIndex); */
    const attributeItem = this.attribute[this.currentAdIndex];
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(attributeItem.component);

    const viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    console.log(componentRef.instance);
    (<AdComponent>componentRef.instance).data = attributeItem.data;
  }
}
