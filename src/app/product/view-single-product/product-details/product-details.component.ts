import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { Color } from './../../../shared/model/colorSetting.model';
import { Child } from './../../../shared/model/child.model';
import { SizeGuide } from '../../../shared/model/sizeGuide.model';
import { Attribute } from '../dynamic-attribute/attribute';
import { PopUpSizeGuideService } from '../pop-up-size-guide/pop-up-size-guide.service';
declare const FB: any;

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})

export class ProductDetailsComponent implements OnInit {
  @Input() productModel: Product;
  @Input() color: Color;
  @Input() selectedSize: boolean;
  @Output() selectedVariant = new EventEmitter<any>();
  @Output() addTocart = new EventEmitter<any>();
  @Output() productImageSelected =  new EventEmitter<any>();
  @Input() productSizeGuide: SizeGuide;
  @Input() selectedColor: string;
  @Input() attribute: Attribute;
  selectedItem: Child;
  showDescription = false;
  char = 120;
  textData;
  file: any;
  constructor(private  popUpSizeGuideService: PopUpSizeGuideService) { 
    console.log(this.productModel);
  }

  ngOnInit() {
    /* console.log(this.productModel);
    this.file = this.productModel; */
    /* this.getDescription(); */
    (window as any).fbAsyncInit = function() {
   
      FB.init({
        appId: '2492070680877764',
        cookie: false,  // enable cookies to allow the server to access
        // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.5
    });
  
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    }
  }


  fb(){
    console.log("hello");
    // FB.ui({
    //   method: 'feed',
    //   // href: urlHost+link,
    //   // method: 'feed',
    //   title: name,
    //   link: "www.ucchal.com",
    //   // picture: urlHost+"/assets/images/fb.jpg",
    //   caption: "Interbank",
    //   description:"des"
    // });
  }

  sizeSelect(itemselect: Child) {
    this.selectedItem = itemselect;
  
    this.selectedVariant.emit(this.selectedItem);
  /*   this.selectedItem.selectSize = itemselect.sizeName; */
  }
  addToCartItem(proId) {
    /* if(this.selectedColor){
      const prod = this.productModel.child.find(el => el.colorId === this.selectedColor);
      this.selectColorSize(prod);
    } */
    this.addTocart.emit(proId);
  }
  selectColorSize(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  }
  selectColor(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  }
  /* getDescription() {
    if (this.showDescription === false) {
      console.log(this.productModel);
      const file = this.productModel.productDescription;
      this.textData = file.substr(0, this.char);
      console.log(this.textData);
    } else {
      this.textData = this.productModel.productDescription;
    }
  } */
  /* checkLoginUser(product) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.WishList.emit(product);
    } else {
      this.router.navigate(['/account/acc/signin']);
    }
  } */
  selectImageModel(event) {
    /* console.log(event); */
    this.productImageSelected.emit(event);
  }
  getPopUp() {
    this.popUpSizeGuideService.openPopUp(this.productSizeGuide);
  }
  readMore() {
    this.showDescription = true;
  }
  readLess() {
    this.showDescription = false;
  }
}
