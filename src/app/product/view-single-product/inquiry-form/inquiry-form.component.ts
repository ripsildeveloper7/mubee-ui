import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialogConfig } from '@angular/material';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../product.service';
import {Inquiry} from '../../../shared/model/inquiry.model';

@Component({
  selector: 'app-inquiry-form',
  templateUrl: './inquiry-form.component.html',
  styleUrls: ['./inquiry-form.component.css']
})
export class InquiryFormComponent implements OnInit {
  country: any;
  InquiryForm : FormGroup;
  inquiryModel: Inquiry;
  constructor(private fb : FormBuilder, public dialogRef: MatDialogRef<InquiryFormComponent>, private productservice: ProductService) { }

  ngOnInit() {
    this.InquiryForm = this.fb.group({
      Name: [''],
      country:[''],
      email: [''],
      mobile: [''],
      inquiry:['']
    });
    this.productservice.getCountry()
    .subscribe( data => {
      this.country = data;
      console.log(data);
    });
  }
  
  getState(event){
  
    this.country.forEach(element => {
     console.log(event);
    //   if(element.state === event.name.value){
    //     console.log(element)
        
       
    // }
    // this.states.map(e => console.log(e));
  })
}
 
  close() {
    this.dialogRef.close("Thanks for using me!");
  }

  // save the date
saveInquiry() {
  this.inquiryModel = new Inquiry();
  this.inquiryModel.country = this.InquiryForm.controls.country.value;
  this.inquiryModel.customerName = this.InquiryForm.controls.Name.value,
  this.inquiryModel.description = this.InquiryForm.controls.inquiry.value,
  this.inquiryModel.emailId = this.InquiryForm.controls.email.value,
  this.productservice.saveInquiry(this.inquiryModel).subscribe(data => {
    console.log(data);
  }, err => {
console.log(err);
  });

  this.close();
} 
}
