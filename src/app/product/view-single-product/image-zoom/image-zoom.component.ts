import { Component, OnInit, ViewChild, Input, ElementRef, Renderer2, HostListener } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from './../../../shared/model/product.model';

@Component({
  selector: 'app-image-zoom',
  templateUrl: './image-zoom.component.html',
  styleUrls: ['./image-zoom.component.css']
})
export class ImageZoomComponent implements OnInit {

  // @Input() imgPath: string;

  // @ViewChild('element', {static:false}) element: ElementRef;

  // public img;
  // public parentSize;

  // constructor() {
  // }
  // ngOnInit(){}

  // ngAfterViewInit() {
  //   this.img = document.createElement('img');
  //   this.img.src = this.imgPath;
  //   console.log(`${this.img.width}x${this.img.height} img: `, this.img);
    
  //   this.parentSize = {
  //     element: this.element.nativeElement.parentNode.parentNode,
  //     get width() {
  //       return this.element.offsetWidth;
  //     },
  //     get height() {
  //       return this.element.offsetHeight;
  //     },
  //   }
  //   console.log(`${this.parentSize.width}x${this.parentSize.height} img: `, this.parentSize);

  //   this.element.nativeElement.style.width = `${this.parentSize.width}px`;
  //   this.element.nativeElement.style.height = `${this.parentSize.height}px`;
  // }

  // public imgZoom(event, zoom = 1): void {
  //   const img = event.target.children[0];

  //   const parentHeight = event.target.offsetHeight;
  //   const parentWidth = event.target.offsetWidth;
  //   const posX = event.layerX;
  //   const posY = event.layerY;

  //   img.style.width = `${parentWidth * zoom}px`;
  //   console.log('parentWidth * zoom: ', img.width = parentWidth * zoom);
  //   img.style.height = `${parentHeight * zoom}px`;
  //   console.log('parentHeight * zoom: ', img.height = parentHeight * zoom);

  //   console.log(`\nx: ${posX} | y: ${posY}`);

  //   console.log('img.width: ', img.width);

  //   img.style.left = `-${(event.layerX * (zoom - 1))}px`;
  //   img.style.top = `-${(event.layerY * (zoom - 1))}px`;
  // }

  // public resetImgZoom(event, zoom = 1): void {
  //   const img = event.target.children[0];

  //   const parentHeight = event.target.offsetHeight;
  //   const parentWidth = event.target.offsetWidth;
  //   img.style.width = `${parentWidth}px`;
  //   img.style.height = `${parentHeight}px`;

  //   event.target.children[0].style.top = `${0}px`;
  //   event.target.children[0].style.left = `${0}px`;
  // }


  @Input('img') imagen: string;
  @Input() zoom=2;
  @Input() lenSize=40;
  @Input() imgWidth;
  @Input() imgHeigth;
  @Input() divZoomed:ElementRef;
  @Input() discount: any;
  
  posX:number=0;
  posY:number=0;
  cx:number=1;
  cy:number=1;
  yet:boolean=false;
  factorX:number;
  factorY:number;
  imageLoader =  true;
  
  private mouseMovement = new Subject();

  @ViewChild('img',{static:false,read:ElementRef}) img
  @ViewChild('len',{static:false,read:ElementRef}) lens
  @HostListener('mousemove',['$event'])
  mouseMove(event:any)
  {
    const result=this.moveLens(event);
    this.render.setStyle(this.divZoomed,'background-position',result)
  }

  constructor(private render:Renderer2){
    
  }
  ngOnInit(){

  }
  imageLoad(){
    this.imageLoader = false;
  }
  onLeave(){
    this.render.setStyle(this.divZoomed,'display','none');
  }
  onLoad()
  {
    // this.render.setStyle(this.divZoomed,'display','block');
    this.render.setStyle(this.divZoomed,'background-image',"url('" + this.imagen+ "')");
    this.render.setStyle(this.divZoomed,'background-size',(this.img.nativeElement.width * this.zoom) + "px " + (this.img.nativeElement.height * this.zoom) + "px")
    this.render.setStyle(this.divZoomed,'background-repeat', 'no-repeat')
    this.render.setStyle(this.divZoomed,'transition','background-position .2s ease-out');
    this.render.setStyle(this.divZoomed,'position','absolute')
    this.render.setStyle(this.divZoomed,'filter','brightness(100%)')
    this.factorX=this.img.nativeElement.width;
    this.factorY=this.img.nativeElement.height;
     this.yet=true;
     setTimeout(()=>{
        this.factorX=this.imgWidth || this.imgHeigth?this.factorX/this.img.nativeElement.width:1
        this.factorY=this.imgWidth || this.imgHeigth?this.factorY/this.img.nativeElement.height:1
    const dim=(this.divZoomed as any).getBoundingClientRect()
    this.cx=(dim.width-this.img.nativeElement.width*this.zoom*this.factorX)/(this.img.nativeElement.width - this.lens.nativeElement.offsetWidth);
    this.cy=(dim.height-this.img.nativeElement.height*this.zoom*this.factorY)/(this.img.nativeElement.height -
     this.lens.nativeElement.offsetHeight);
         


     })


  }
  moveLens(e:any)
  {
    let pos
    let x
    let y;
    /*prevent any other actions that may occur when moving over the image:*/
    e.preventDefault();
    /*get the cursor's x and y positions:*/
    pos = this.getCursorPos(e);
    /*calculate the position of the lens:*/
    x = pos.x - (this.lens.nativeElement.offsetWidth / 2);
    y = pos.y - (this.lens.nativeElement.offsetHeight / 2);
    /*prevent the lens from being positioned outside the image:*/
    if (x > this.img.nativeElement.width - this.lens.nativeElement.offsetWidth) {x = this.img.nativeElement.width - this.lens.nativeElement.offsetWidth;}
    if (x < 0) {x = 0;}
    if (y > this.img.nativeElement.height - this.lens.nativeElement.offsetHeight) {y = this.img.nativeElement.height - this.lens.nativeElement.offsetHeight;}
    if (y < 0) {y = 0;}
    /*set the position of the lens:*/
    this.posX = x;
    this.posY = y;
    /*display what the lens "sees":*/

    let result = (x * this.cx) + "px "+(y * this.cy) + "px"

    return result;


  }
  getCursorPos(e) {
    let a, x = 0, y = 0;
    e = e || window.event;
    /*get the x and y positions of the image:*/
    a = this.img.nativeElement.getBoundingClientRect();
    /*calculate the cursor's x and y coordinates, relative to the image:*/
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /*consider any page scrolling:*/
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}
