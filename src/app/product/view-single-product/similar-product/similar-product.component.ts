import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductCart } from '../../../shared/model/productCart.model';
@Component({
  selector: 'app-similar-product',
  templateUrl: './similar-product.component.html',
  styleUrls: ['./similar-product.component.css']
})
export class SimilarProductComponent implements OnInit {
  @Input() recentPrdouctData: any;
  showMobileView = false;
    constructor(private router: Router) { }
  
    ngOnInit() {
      if (window.screen.width > 900) {
        this.showMobileView = false;
      } else {
        this.showMobileView = true;
      }
    }
    ngAfterViewInit() {
      this.getWindowSize();
    }
    getWindowSize() {
      if (window.screen.width > 900) {
        this.showMobileView = false;
      } else {
        this.showMobileView = true;
      }
    }

    getProduct(product) {
      if ( product.superCategoryId &&  product.mainCategoryId &&  product.subCategoryId) {
        this.router.navigate(['/product/viewsingle/',  product.subCategoryId, product._id]);
      } else if ( product.superCategoryId &&  product.mainCategoryId) {
        this.router.navigate(['/product/viewsingle/',  product.mainCategoryId, product._id]);
      } else if ( product.superCategoryId) {
        this.router.navigate(['/product/viewsingle/',  product.superCategoryId, product._id]);
      }
  
    }
    getView(id, supId, subId) {
      if(subId){
        this.router.navigate(['product/viewsingle/', subId, id ]);
      } else {
        this.router.navigate(['product/viewsingle/', supId, id ]);
      }
    }
}

