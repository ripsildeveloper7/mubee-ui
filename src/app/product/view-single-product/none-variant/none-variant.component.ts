import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
@Component({
  selector: 'app-none-variant',
  templateUrl: './none-variant.component.html',
  styleUrls: ['./none-variant.component.css']
})
export class NoneVariantComponent implements OnInit {
  @Output() addTocart = new EventEmitter<any>();
  @Input() productModel: Product;
  constructor() { }

  ngOnInit() {
  }
  addToCartItem(proId) {
    /* if(this.selectedColor){
      const prod = this.productModel.child.find(el => el.colorId === this.selectedColor);
      this.selectColorSize(prod);
    } */
    console.log(proId);
    this.addTocart.emit(proId);
  }
}
