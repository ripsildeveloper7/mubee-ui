import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, AfterViewInit, HostListener } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { group, trigger, transition, query, style, animate } from '@angular/animations';
@Component({
  selector: 'app-product-image',
  templateUrl: './product-image.component.html',
  styleUrls: ['./product-image.component.css'],
  animations: [
    trigger('slideAnimation', [
      transition(':increment ,:decrement', group([
        query(':enter', [
          style({
            transform: 'translateX(100%)'
          }),
          animate('1s ease-out',)
        ])
      ])
    )
  ])
] 
})
export class ProductImageComponent implements OnInit, AfterViewInit {
  @Input() productModelImage: any;
  @Input() productModel: Product;
  @Input() productImageUrl: string;
  @Output() clickOnImage = new EventEmitter<any>();
  slideIndex = 0;
  displayMobile = false;
  interval: any;
  imageLoader = true;
  show= false;
 
  constructor() { 
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }

  ngOnInit() {
    /* this.getAds(); */
  }
  onLoad(){
    this.imageLoader = false;
  }
  mouser(){
    this.show = true;
  }
  over(){
    // document.getElementById('a').style.display = "block";
    this.show = false;
  }
  
  /* getAds() {
    this.interval = setInterval(() => {
      this.loadComponent();
    }, 1000);
  } */
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }
  /* loadComponent(){
    console.log(this.productModelImage);
    if (this.slideIndex >= this.productModelImage.length - 1) {
      this.slideIndex = 0;
    } else {
      this.slideIndex = this.slideIndex + 1;
    }
    this.productModelImage.forEach((element, index) => {
      if (index === this.slideIndex) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  } */
  selectedZoom(sildeIndex) {
  this.clickOnImage.emit(sildeIndex);
  }
  clickRight(array, slideIndex) {
    if (slideIndex >= array.length - 1) {
      this.slideIndex = 0;
    } else {
      this.slideIndex = slideIndex + 1;
    }
    array.forEach((element, index) => {
      if (index === this.slideIndex) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  }
  clickCenter(array, sildeIndex){
    this.slideIndex = sildeIndex;
    array.forEach((element, index) => {
      if (index === sildeIndex) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  }
  clickLeft(array, indexValue) {
    if (indexValue <= 0) {
      this.slideIndex = array.length - 1 ;
    } else {
      this.slideIndex = indexValue - 1;
    }
    array.forEach((element, index) => {
      if (index === this.slideIndex) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  }


  // constructor(private shareservice:SharedService) { }

  defaultTouch = { x: 0, y: 0, time: 0 };

  @HostListener('touchstart', ['$event'])
  //@HostListener('touchmove', ['$event'])
  @HostListener('touchend', ['$event'])
  @HostListener('touchcancel', ['$event'])
  handleTouch(event) {
      let touch = event.touches[0] || event.changedTouches[0];

      // check the events
      if (event.type === 'touchstart') {
          this.defaultTouch.x = touch.pageX;
          this.defaultTouch.y = touch.pageY;
          this.defaultTouch.time = event.timeStamp;
      } else if (event.type === 'touchend') {
          let deltaX = touch.pageX - this.defaultTouch.x;
          let deltaY = touch.pageY - this.defaultTouch.y;
          let deltaTime = event.timeStamp - this.defaultTouch.time;

          // simulte a swipe -> less than 500 ms and more than 60 px
          if (deltaTime < 500) {
              // touch movement lasted less than 500 ms
              if (Math.abs(deltaX) > 60) {
                  // delta x is at least 60 pixels
                  if (deltaX > 0) {
                      this.doSwipeRight(event);
                  } else {
                      this.doSwipeLeft(event);
                  }
              }

              if (Math.abs(deltaY) > 60) {
                  // delta y is at least 60 pixels
                  if (deltaY > 0) {
                      this.doSwipeDown(event);
                  } else {
                      this.doSwipeUp(event);
                  }
              }
          }
      }
  }

  doSwipeLeft(event) {
      console.log('swipe left', event);
      this.clickLeft(this.productModelImage, this.slideIndex);
  }

  doSwipeRight(event) {
      console.log('swipe right', event);
      // this.nav.toggleNavbar();
      this.clickRight(this.productModelImage, this.slideIndex);
  }

  doSwipeUp(event) {
      console.log('swipe up', event);
  }

  doSwipeDown(event) {
      console.log('swipe down', event);
  }
}
