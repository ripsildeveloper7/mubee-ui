import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/shared/model/product.model';

@Component({
  selector: 'app-product-title',
  templateUrl: './product-title.component.html',
  styleUrls: ['./product-title.component.css']
})
export class ProductTitleComponent implements OnInit {
  @Input() productModel: Product;

  constructor() { }

  ngOnInit() {
  }

}
