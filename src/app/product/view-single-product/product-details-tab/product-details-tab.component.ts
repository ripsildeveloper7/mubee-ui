import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { Attribute } from './../dynamic-attribute/attribute';
@Component({
  selector: 'app-product-details-tab',
  templateUrl: './product-details-tab.component.html',
  styleUrls: ['./product-details-tab.component.css']
})
export class ProductDetailsTabComponent implements OnInit, AfterViewInit {
  tabItems = [{item: 'Description'}, {item: 'Product Details'}];
  @Input() productModel: Product[];
  @Input() attribute: Attribute[];
  selectedItemTab = this.tabItems[0].item;
  @Input() productImageUrl: string;
  displayMobile = false;
  constructor() { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }
  selectedTab(tab) {
    this.selectedItemTab = tab;
  }
}
