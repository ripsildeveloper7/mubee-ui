import { Component, OnInit, Inject, Optional, Input  } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { SizeGuide } from '../../../shared/model/sizeGuide.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppSetting } from '../../../config/appSetting';


@Component({
  selector: 'app-pop-up-size-guide',
  templateUrl: './pop-up-size-guide.component.html',
  styleUrls: ['./pop-up-size-guide.component.css']
})
export class PopUpSizeGuideComponent implements OnInit {
  sizeGuideImageUrl = AppSetting.sizeGuideImageUrl;
  isCm = false;
  isInches = true;
  constructor(private fb: FormBuilder,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<PopUpSizeGuideComponent>) { }

  ngOnInit() {
    console.log(this.data);
  }
  close() {
    this.dialogRef.close(true);
  }
  radioChange(num) {
    
    if (num.value === '1') {
      this.isCm = true;
      this.isInches = false;
    } else if (num.value === '2') {
      this.isCm = false;
      this.isInches = true;
    }
  }
}
