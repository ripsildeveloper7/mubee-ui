
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { PopUpSizeGuideComponent } from './pop-up-size-guide.component';
import { SizeGuide } from '../../../shared/model/sizeGuide.model';

@Injectable({
  providedIn: 'root'
})
export class PopUpSizeGuideService {
  dialogRef: MatDialogRef<PopUpSizeGuideComponent>;
  constructor(private dialog: MatDialog) { }

  openPopUp(val?: SizeGuide): Observable<boolean> {
    this.dialogRef = this.dialog.open(PopUpSizeGuideComponent,
      {
        disableClose: true, backdropClass: 'light-backdrop',
       /*  width: '1020px',
        height: '420px', */
        width: '550px',
        height:'auto',
        data: val,
      });
    return this.dialogRef.afterClosed();
  }
  closePop() {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
}
