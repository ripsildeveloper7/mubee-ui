import { Component, OnInit, Input } from '@angular/core';
import { Data, ActivatedRoute, Router } from '@angular/router';
import { AppSetting } from 'src/app/config/appSetting';
import { SuperCategory } from 'src/app/shared/model/superCategory.model';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  @Input() commonCategory: SuperCategory;
  @Input() cat: any;
  @Input() brandModel: any;
  @Input() tagData: any;
  @Input() promotionData: any;
  @Input() recentProduct: any;
  categoryImageUrl: string = AppSetting.categoryImageUrl;
  /* mainCategoryImageUrl: string = AppSetting.; */
  subCategoryImageUrl: string = AppSetting.subCategoryImageUrl;
  showMobileView = false;
  constructor() { 
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  // supCatLink(){
  //   this.router.navigate(['/product/supercategory/',this.supCatId])
  // }


}
