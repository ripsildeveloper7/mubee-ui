import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {ProductRoutingModule} from './product-routing.module';
import {
  MatSidenavModule,
  MatListModule,
  MatTooltipModule,
  MatOptionModule,
  MatSelectModule,
  MatMenuModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatRippleModule,
  MatDialogModule,
  MatChipsModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule,
  MatTabsModule,
  MatSliderModule
} from '@angular/material';

import { ProductListComponent } from './product-list/product-list.component';
import { ProductFilterComponent } from './product-filter/product-filter.component';
import { ZoommodelComponent } from './zoommodel/zoommodel.component';
import { CarouselItemComponent } from './carousel-item/carousel-item.component';
import {CarouselComponent} from './carousel/carousel.component';
import { ProductItemComponent } from './view-product/product-item/product-item.component';
import { AllProductComponent } from './view-product/all-product/all-product.component';
import { HoverItemDirective } from './view-product/product-item/hover-item.directive';
import { SideBarComponent } from './view-product/side-bar/side-bar.component';
import { ProductHeaderComponent } from './view-product/product-header/product-header.component';
import { ScrollableDirective } from './view-product/all-product/scroll.directive';
import { ProductImageComponent } from './view-single-product/product-image/product-image.component';
import { ProductDetailsComponent } from './view-single-product/product-details/product-details.component';
import { SingleProductComponent } from './view-single-product/single-product/single-product.component';
import { CarouselItemDirective } from './carousel-item/carousel-item.directive';
import { ProductDetailsTabComponent } from './view-single-product/product-details-tab/product-details-tab.component';
import { ProductCategoryComponent } from './view-product/product-category/product-category.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ProductResolver } from './view-product/guards/product-resolver';
import { CategoryResolver } from './view-product/guards/category-resolver';
import { MainCategoryProductResolver } from './view-product/guards/maincategory-product-resolver';
import { SubCategoryProductResolver } from './view-product/guards/subcategory-product-resolver';
import { AllProductResolver } from './view-product/guards/allproduct-resolver';
import { ProductItemResolver } from './view-single-product/guard/product-item.resolver';
import { ProductZoomComponent } from './view-single-product/product-zoom/product-zoom.component';
import { ProductReviewComponent } from './view-single-product/product-review/product-review.component';
import { ScrollService } from './view-product/product-item/scroll.service';
import { PopUpSizeGuideComponent } from './view-single-product/pop-up-size-guide/pop-up-size-guide.component';
import { PopUpSizeGuideService } from './view-single-product/pop-up-size-guide/pop-up-size-guide.service';
import { WindowScrollingService } from './view-single-product/product-zoom/window-scrolling.service';
import { SimilarProductComponent } from './view-single-product/similar-product/similar-product.component';
import { SizeVariantComponent } from './view-single-product/size-variant/size-variant.component';
import { NoneVariantComponent } from './view-single-product/none-variant/none-variant.component';
import { SharedModule } from './../shared/shared.module';
import { SizeColorVariantComponent } from './view-single-product/size-color-variant/size-color-variant.component';
import { ColorVariantComponent } from './view-single-product/color-variant/color-variant.component';
import { ProductAttributeComponent } from './view-single-product/dynamic-attribute/product-attribute/product-attribute.component';
import { DynamicAttributeComponent } from './view-single-product/dynamic-attribute/dynamic-attribute/dynamic-attribute.component';
import { AttributeDirective } from './view-single-product/dynamic-attribute/attribute.directive';
import { AttributeService } from './view-single-product/dynamic-attribute/attribute.service';
import { BrandResolver } from './view-product/guards/brand-resolver';
import { CollectionResolver } from './view-product/guards/collection-resolver';
import { PromotionResolver } from './view-product/guards/promotion-resolver';
import { RecentProductResolver } from './view-product/guards/recentProduct-resolver';
import { SearchResolver } from './view-product/guards/search-resolver';
import { ProductGuard } from './view-product/guards/product.guard';
import { AttributeFilterComponent } from './view-product/filter/attribute-filter/attribute-filter.component';
import { ImageZoomComponent } from './view-single-product/image-zoom/image-zoom.component';
import { SizeFilterComponent } from './view-product/filter/size-filter/size-filter.component';
import { ColorFilterComponent } from './view-product/filter/color-filter/color-filter.component';
import { AllAttributeResolver } from './view-product/guards/all-attribute-resolver';
import { CategoryProductResolver } from './view-single-product/guard/category-product.resolver';
import { ProductSingleDetailsComponent } from './view-single-product/product-single-details/product-single-details.component';
import { AddToCartComponent } from './view-single-product/add-to-cart/add-to-cart.component';
import { ProductTitleComponent } from './view-single-product/product-title/product-title.component';
import { InquiryFormComponent } from './view-single-product/inquiry-form/inquiry-form.component';
import { InquiryComponent } from './view-single-product/inquiry/inquiry.component';
import { AttributeProductResolver } from './view-product/guards/attribute-product-resolver';
import { NewArriavalResolver } from './view-product/guards/newArrivals-resolver';
import { ProductDisplayComponent } from './view-product/product-display/product-display.component';
// import { PageStyleDirective } from './view-product/product-item/page-style.directive';
import { TestGuardsGuard } from './view-product/guards/test-guards.guard';
import { FilterOptionsComponent } from './view-product/filter/filter-options/filter-options.component';
import { YouMayAlsoLikePageComponent } from './view-single-product/you-may-also-like-page/you-may-also-like-page.component';
import { FilterResolver } from './view-product/guards/filter-resolver';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import
 { SingleCategoryResolver } from './view-product/guards/single-category-resolver';
import { MainCategoryComponent } from './view-product/main-category/main-category.component';
import { CatAttributeResolver } from './view-product/guards/cat-attribute-resolver';
import { CategoryCountResolver } from './view-product/guards/category-count-resolver';
// import { RecentlyViewComponent } from './view-single-product/recently-view/recently-view.component';
// import { MeasurementPageComponent } from './view-single-product/measurement-page/measurement-page.component';
@NgModule({
  declarations: [
      ProductListComponent,
      ProductFilterComponent,
      CarouselItemComponent, CarouselItemDirective, CarouselComponent,
      ZoommodelComponent,
      DynamicAttributeComponent,
      ProductItemComponent,
      AllProductComponent,
      HoverItemDirective,
      SideBarComponent,
      ProductHeaderComponent,
      ScrollableDirective,
      ProductImageComponent,
      ProductDetailsComponent,
      SingleProductComponent,
      ProductDetailsTabComponent,
      ProductCategoryComponent,
      BreadcrumbComponent,
      ProductZoomComponent,
      ProductReviewComponent,
      PopUpSizeGuideComponent,
      SimilarProductComponent,
      SizeVariantComponent,
      NoneVariantComponent,
      SizeColorVariantComponent,
      ColorVariantComponent,
      ProductAttributeComponent,
      AttributeDirective,
      AttributeFilterComponent,
      ImageZoomComponent,
      SizeFilterComponent,
      ColorFilterComponent,
      ProductSingleDetailsComponent,
      AddToCartComponent,
      ProductTitleComponent,
      InquiryFormComponent,
      InquiryComponent,
      ProductDisplayComponent,
      FilterOptionsComponent,
      YouMayAlsoLikePageComponent,
      MainCategoryComponent,
      // RecentlyViewComponent,
      // MeasurementPageComponent
     ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    CommonModule,
    SharedModule,
    ProductRoutingModule,
    ReactiveFormsModule,
    ProductRoutingModule,
    MatNativeDateModule,
    HttpClientModule,
    MatOptionModule,
    FormsModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatRippleModule,
    MatChipsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatMenuModule,
    MatStepperModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatTabsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
    MatSliderModule
  ],
  providers: [ProductResolver, ProductGuard, TestGuardsGuard, CategoryResolver, AttributeService,
              PopUpSizeGuideService, WindowScrollingService,
              MainCategoryProductResolver, SubCategoryProductResolver, NewArriavalResolver,
              CategoryProductResolver, ProductItemResolver, AllProductResolver,
              SingleCategoryResolver,
              BrandResolver, AllAttributeResolver, AttributeProductResolver,
              FilterResolver, CatAttributeResolver, CategoryCountResolver,
              CollectionResolver, PromotionResolver, RecentProductResolver, SearchResolver, ScrollService],
      entryComponents: [PopUpSizeGuideComponent, ProductAttributeComponent, DynamicAttributeComponent, 
                        InquiryFormComponent]
})
export class ProductModule { }
