import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Zoom } from './zoom.model';
import { Banner } from './banner.model';
import { ProductService } from './../product.service';
@Component({
  selector: 'app-zoommodel',
  templateUrl: './zoommodel.component.html',
  styleUrls: ['./zoommodel.component.css']
})
export class ZoommodelComponent implements OnInit {
  /* @Input() zoom: Zoom;
  @Output() closeZoom = new EventEmitter<any>(); */
  fileToUpload;
  fileToLoad;
  fileLength;
  banner: Banner;
  bannerTest: Banner;
  bannerData;
  byteArrayConverted: any;
  check;
  showImage: any;
  BASE64_MARKER: any = ';base64,';
  message;
  action;
  constructor(private productService: ProductService) { }

  ngOnInit() {
  }
  /* closed() {
    this.zoom.clickIndex = false;
    this.closeZoom.emit();
  } */
  handleFileInput(files: FileList) {
    this.fileToUpload = files[0];
    this.banner = new Banner();
    const reader = new FileReader();
    const file = this.fileToUpload;
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.check = reader.result;
      console.log(this.fileToUpload.name);
      console.log(this.check);
      this.byteArrayConverted = this.convertDataURIToBinary(this.check);
      this.banner.bannerName = this.fileToUpload.name;
      this.banner.bannerImageName = this.check;
      console.log(this.banner);
      this.showImage = this.check;
    };
    this.banner = this.banner;
    console.log(this.banner);
  }
  convertDataURIToBinary(dataURI) {
    const base64Index = dataURI.indexOf(this.BASE64_MARKER) + this.BASE64_MARKER.length;
    const base64 = dataURI.substring(base64Index);
    const raw = window.atob(base64);
    const rawLength = raw.length;
    const array = new Uint8Array(new ArrayBuffer(rawLength));
    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }
  uploadBanner() {
    this.productService.uploadBannerImage(this.banner).subscribe(data => {
      this.bannerTest  = data;
      console.log(this.bannerTest);
    }, error => {
      console.log(error);
    });
  }
}
