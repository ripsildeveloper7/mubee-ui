import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { MainCategory } from './../../../shared/model/mainCategory.model';
import {  SuperCategory } from './../../../shared/model/superCategory.model';
import { AppSetting } from './../../../config/appSetting';
@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.css'],
  /* animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    )] */
})
export class ProductCategoryComponent implements OnInit, AfterViewInit {
  @Input() commonCategory: SuperCategory;
  @Input() cat: any;
  @Input() brandModel: any;
  @Input() tagData: any;
  @Input() promotionData: any;
  @Input() recentProduct: any;
  categoryImageUrl: string = AppSetting.categoryImageUrl;
  /* mainCategoryImageUrl: string = AppSetting.; */
  subCategoryImageUrl: string = AppSetting.subCategoryImageUrl;
  showMobileView = false;
  constructor() { 
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

}
