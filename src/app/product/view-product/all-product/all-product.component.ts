import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { AppSetting } from './../../../config/appSetting';
import { ProductService } from './../../../product/product.service';
import { Product } from './../../../shared/model/product.model';
import { MainCategory } from './../../../shared/model/mainCategory.model';
import { BrandModel } from './../../../shared/model/brand.model';
import { Color } from './../../../shared/model/colorSetting.model';
import { ProductSettings } from './../../../shared/model/product-setting.model';
import { Filter } from './../all-product/filter.model';
import { CommonFilter } from './../all-product/commonFilter.model';
import {
  ActivatedRoute, Router, NavigationEnd, NavigationStart,
  PRIMARY_OUTLET, RoutesRecognized, Params, ParamMap, Data, Event, ActivatedRouteSnapshot
} from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { SuperCategory } from 'src/app/shared/model/superCategory.model';
import { ProductOption } from 'src/app/shared/model/product-option.model';
import { SubCategory } from 'src/app/shared/model/sub-category.model';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Price } from '../filter/filter-options/price.model';


@Component({
  selector: 'app-all-product',
  templateUrl: './all-product.component.html',
  styleUrls: ['./all-product.component.css']
})
export class AllProductComponent implements OnInit, OnDestroy, AfterViewInit {
  productImageUrl: string = AppSetting.productImageUrl;
  productModel: Product;
  productOptionModel: ProductOption;
  productCommonModel: Product[];
  childParamSubscription: Subscription;
  childQueryParamSubscription: Subscription;
  mainCategory: MainCategory;
  superCategory: SuperCategory;
  singleSuperCategory: SuperCategory;
  subCategory: SubCategory;
  brandModel: BrandModel;
  productSettings: ProductSettings;
  color: Color[];
  colorSchema: Array<any> = [];
  nullColor: any;
  filter: Filter;
  public pageSize = 50;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  cat: {
    catId?: string, mainCatId?: string, subCatId?: string, brandId?: string,
    collectionId?: string, promotionId?: string, recentProductId?: string, searchID?: string
  };
  promotionData: { title: string, description: string, promotion: boolean };
  recentProduct: { title: string, recent: boolean };
  breadcrumbs;
  commonCategory: SuperCategory[];
  mainCatId: string;
  subCatId: string;
  id: string;
  private paramSubscription: any;
  attributeModel: Array<any> = [];
  showMobileView = false;
  showMobileView1 = false;
  showMobileView2 = false;
  brandId: string;
  collectionId: string;
  promotionId: string;
  searchID: string;
  tagData;
  promotionName;
  promotionDesc;
  promotion = false;
  filterOptions: Array<any> = [];
  fieldAttributePush: Array<any> = [];
  result: any;
  breadCrumbDetails: string;
  filterOptionsData: any;
  commonResult: CommonFilter;
  navigationSubscription;
  display: string;
  count: any;
  constructor(private productService: ProductService,  private activatedRoute: ActivatedRoute, private router: Router) {
    this.id = this.activatedRoute.snapshot.paramMap.get('catid');
    this.cat = {
      catId: this.id,
      
      /* mainCatId: this.activatedRoute.firstChild.snapshot.,
      subCatId: this.activatedRoute.firstChild.snapshot.params.get('subid'),
      brandId: this.activatedRoute.firstChild.snapshot.params.get('brandid'),
      collectionId: this.activatedRoute.firstChild.snapshot.params.get('collectionid'),
      promotionId: this.activatedRoute.firstChild.snapshot.params.get('promotionid'),
      recentProductId: this.activatedRoute.firstChild.snapshot.params.get('recentid'),
      searchID: this.activatedRoute.firstChild.snapshot.params.get('id'), */
    };
     /* router.events.subscribe((event: Event) => {
       if (event instanceof NavigationEnd) {
        
       }
     }); */
     this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('catid');
    });
    
    this.childParamSubscription = this.activatedRoute.firstChild.paramMap.subscribe(
      (params: ParamMap): void => {
        this.fieldAttributePush = new Array();
        this.commonResult = new CommonFilter(); 
        this.result = {};
        console.log(this.activatedRoute.snapshot);
        console.log(params, 'test 8')
        this.cat = {
          catId: params.get('catid') === '' ? this.id: this.id,
          mainCatId: params.get('maincatid'),
          subCatId: params.get('subid'),
          brandId: params.get('brandid'),
          collectionId: params.get('collectionid'),
          promotionId: params.get('promotionid'),
          recentProductId: params.get('recentid'),
          searchID: params.get('id'),
        };
        this.viewSingleCategory();
      });
  }
  ngOnDestroy() {
    this.childParamSubscription.unsubscribe();
  }
  ngOnInit() {
    this.activatedRoute.data.subscribe((data: Data) => {
      this.activatedRoute.children[0].paramMap.subscribe((params: ParamMap) => {
        this.cat = {
          catId: params.get('catid') === '' ? this.id: this.id ,
          mainCatId: params.get('maincatid'),
          subCatId: params.get('subid'),
          /* attributeId: params.get('attribute'),
          attributefiledId: params.get('attributefiledId') */
          brandId: params.get('brandid'),
          collectionId: params.get('collectionid'),
          promotionId: params.get('promotionid'),
          recentProductId: params.get('recentid'),
          searchID: params.get('id'),
        };
      });
      this.count = data.count;
  /*     console.log(this.activatedRoute.children[0].paramMap.subscribe((params: ParamMap) => {
        this.id = params.get('catid');
      }), 'new local'); */
         this.fieldAttributePush = new Array();
        this.commonResult = new CommonFilter();
      this.productModel = new Product();
      this.mainCategory = new MainCategory();
      this.singleSuperCategory = new SuperCategory();
      this.subCategory = new SubCategory();
      // this.superCategory = data.category;
      // this.filterOptions = data.filterOption;
      // this.attributeModel = data.attribute;
      this.superCategory = data.common.category;
      console.log(data.common,"common")
      this.filterOptions = data.common.filterOption;
      this.attributeModel = data.common.filterAttribute.sort((a,b) => 
      a.sortOrder - b.sortOrder
    );
    console.log(this.attributeModel,"model attr")
      // this.filterOptions[0].filterDispatchValue = this.filterOptions[0].filterDispatchValue.sort((a,b)=> a.day-b.day);
      //  this.filterOptions[0].filterOptionDiscountValue = this.filterOptions[0].filterOptionDiscountValue.sort((a,b)=> a.minDiscount-b.minDiscount);
      // console.log('aaa',this.filterOptions);
      
   
   
      /* this.viewSingleCategory(); */
      /* this.commonCategory = data.common; */
      this.id = this.activatedRoute.snapshot.paramMap.get('catid');
      
     /*  this.cat = {
        catId: this.id,
        mainCatId: this.activatedRoute.firstChild.snapshot,
        subCatId: this.activatedRoute.firstChild.snapshot.paramMap.get('subid'),
        brandId: this.activatedRoute.firstChild.snapshot.paramMap.get('brandid'),
        collectionId: this.activatedRoute.firstChild.snapshot.paramMap.get('collectionid'),
        promotionId: this.activatedRoute.firstChild.snapshot.paramMap.get('promotionid'),
        recentProductId: this.activatedRoute.firstChild.snapshot.paramMap.get('recentid'),
        searchID: this.activatedRoute.firstChild.snapshot.paramMap.get('id'),
      }; */
      this.viewSingleCategory()
      /* console.log(this.commonCategory, 'test 3'); */
    });

    /*   setTimeout(() => {
        this.getQueryParams();
      }); */
    /*  if (this.cat.promotionId) {
       this.promotionData = {
         title: this.activatedRoute.snapshot.queryParams.name,
         description: this.activatedRoute.snapshot.queryParams.desc,
         promotion: true
       };
     } */
  }

  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView1 = false;
    } else {
      this.showMobileView1 = true;
    }
  }

  toggleDropdown() {
    this.showMobileView = !this.showMobileView;
  }
  filterOptionsDataEvent(data) {
    
    this.commonResult.discount = data.discount;
    this.commonResult.dispatch = data.dispatch;
    this.commonResult.maxPrice = data.maxPrice;
    this.commonResult.minPrice = data.minPrice;
    /* this.commonResult.dispatch = data.dispatch === undefined ? {} : data.dispatch;
    this.commonResult.dis = data.dis === undefined ? {} : data.dis;
    this.commonResult.maxPrice = data.maxPrice === undefined ? {} : data.maxPrice;
    this.commonResult.minPrice = data.minPrice === undefined ? {} : data.minPrice; */
    this.result = this.result === undefined ? {} : this.result;
    var resultMerge = Object.assign(this.result, this.commonResult);
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
        resultMerge
    });
  }
  toggleDropdown1() {
    this.showMobileView2 = !this.showMobileView2;
  }
  getQueryParams(): void {
    this.activatedRoute.queryParamMap.subscribe((queryparams: ParamMap): void => {
      this.nullColor = queryparams.get('color');
      this.nullColor = this.nullColor === null ? [] : this.nullColor.split(',');
      this.colorSchema.forEach(item => {
        this.nullColor.find(test => {
          if (test === item.name) {
            item.nameChecked = true;
          }
        });
      });
    });
  }
  filterMultiAll(colorData) {
    if (colorData.color.length === 0 || colorData.size.length === 0 || colorData.price.maxPrice === undefined
      || colorData.price.minPrice === null) {
      const nullColor = colorData.color.length === 0 ? null : colorData.color.toString(',');
      const nullSize = colorData.size.length === 0 ? null : colorData.size.toString(',');
      var dataMerge = {
        color: nullColor,
        size: nullSize,
        maxPrice: colorData.price.maxPrice,
        minPrice: colorData.price.minPrice,
      }
      
      /* if(colorData.attribute.length > 0) {
        for (var i = 0; i < colorData.attribute.length; i++) {
          result[colorData.attribute[i].fieldName] = colorData.attribute[i].fieldValue;
        }
      } */
      /*  let params = new HttpParams();
       colorData.attribute.forEach((test) =>{
       params = params.append(test.type, test.correct);
       }); */
      /* console.log(params); */
      var resultMerge = Object.assign(dataMerge, colorData.attribute);
      this.router.navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: resultMerge
      });
    } else {
      this.router.navigate([], {
        relativeTo: this.activatedRoute, queryParams: {
          color: colorData.color.toString(','),
          size: colorData.size.toString(','),
          maxPrice: colorData.price.maxPrice,
          minPrice: colorData.price.minPrice
        }
      });
    }
  }
  viewAllBrand() {
    this.productService.getAllBrand().subscribe(data => {
      this.brandModel = data;
      this.getAllProductOption();
    }, err => {
      console.log(err);
    });
  }

  viewFilterOptions() {
    this.productService.getFilterOptions().subscribe(data => {
      this.filterOptions = data;
    }, err => {
      console.log(err);
    });
  }
  viewAllCategory() {
    this.productService.getAllCategory().subscribe(data => {
      this.superCategory = data;
    }, err => {
      console.log(err);
    });
  }
  getAllProductOption() {
    this.productService.allProductOption().subscribe(data => {
      this.productOptionModel = data;
    }, error => {
      console.log(error);
    });
  }
  viewSingleCategory() {
    /*   if (this.cat.promotionId) {
        this.promotionData = {
          title: this.activatedRoute.snapshot.queryParams.name,
          description: this.activatedRoute.snapshot.queryParams.desc,
          promotion: true
        };
        this.recentProduct.recent = false;
        this.filter.categoryFilter = false;
        this.filter.subCategoryFilter = false;
      } else {
        this.promotionData = {
          promotion: false,
          title: 'string',
          description: 'string'
        };
      }
      if (this.cat.recentProductId) {
        this.recentProduct = {
          title: 'Recently Viewed',
          recent: true
        };
      } else {
        this.recentProduct = {
          title: 'Recently Viewed',
          recent: false
        };
      } */
      
    this.productService.getSingleCategory(this.id).subscribe(data => {
      this.commonCategory = [];
      this.commonCategory.push(this.superCategory);
      this.commonCategory.forEach((ele) => {
          if (this.cat.catId && this.cat.mainCatId && this.cat.subCatId) {
            ele.superCatData = false;
            ele.mainCatData = false;
            ele.brandCatData = false;
            ele.collectionCatData = false;
            ele.promotionCatData = false;
            ele.subCatData = true;
          } else if (this.cat.catId && this.cat.mainCatId) {
            ele.superCatData = false;
            ele.mainCatData = true;
            ele.brandCatData = false;
            ele.collectionCatData = false;
            ele.promotionCatData = false;
            ele.subCatData = false;
          } else if (this.cat.brandId) {
            ele.brandCatData = true;
            ele.superCatData = false;
            ele.mainCatData = false;
            ele.subCatData = false;
            ele.collectionCatData = false;
            ele.promotionCatData = false;
            ele.brandId = this.cat.brandId;
          } else if (this.cat.collectionId) {
            ele.collectionCatData = true;
            ele.brandCatData = false;
            ele.superCatData = false;
            ele.mainCatData = false;
            ele.subCatData = false;
            ele.promotionCatData = false;
            ele.collectionId = this.cat.collectionId;
          } else if (this.cat.catId) {
            ele.promotionCatData = false;
            ele.superCatData = true;
            ele.mainCatData = false;
            ele.subCatData = false;
            ele.brandCatData = false;
            ele.collectionCatData = false;
          }
          this.breadCrumbDetails = ele.categoryName;
        });
      if (this.cat.mainCatId) {
        this.viewSingleMainCategory();
      } else {

      }
      
      /* this.commonCategory = new SuperCategory();
      this.commonCategory.categoryName = this.superCategory.categoryName;
      this.commonCategory.categoryDescription = this.superCategory.categoryDescription;
      this.commonCategory = this.commonCategory; */
    }, err => {
      console.log(err);
    });
  }
  filterAttributeMultiAll(colorData) {
    /* this.result = {};
    if (colorData.attribute.length > 0) {
      for (var i = 0; i < colorData.attribute.length; i++) {
        if (colorData.attribute[i].fieldValue.length > 0) {
          this.result[colorData.attribute[i].fieldName] = colorData.attribute[i].fieldValue.toString(',');
        }
      }
    }
    if(!this.commonResult){
      this.commonResult = new CommonFilter(); 
  } else {
    
    this.commonResult.price = this.commonResult.price === undefined ? {} : this.commonResult.price;
    this.commonResult.dis = this.commonResult.dis === undefined ? {} : this.commonResult.dis;
    this.commonResult.dispatch = this.commonResult.dispatch === undefined ? {} : this.commonResult.dispatch;
  }
    var resultMerge = Object.assign(this.commonResult, this.result);
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
        resultMerge
    });
     */
    
    this.result = {}; 
    if(colorData.attribute.length > 0) {
      for (var i = 0; i < colorData.attribute.length; i++) {
        if(colorData.attribute[i].fieldValue.length > 0){
        this.result[colorData.attribute[i].fieldName] = colorData.attribute[i].fieldValue.toString(',');
       }
      }
    }
    var resultMerge = Object.assign(this.result, this.commonResult);
     this.router.navigate([], {
       relativeTo: this.activatedRoute, queryParams: 
       resultMerge
     });
  }
  subCategoryAttribute()   { 
    this.productService.getSubCategoryAttribute(this.cat.subCatId).subscribe(data => {
    this.attributeModel = data;
  }, err => {
    console.log(err);
  });
}
  superCategoryAttribute() { 
      this.productService.getSuperCategoryAttribute(this.cat.catId).subscribe(data => {
      this.attributeModel = data;
    }, err => {
      console.log(err);
    });
  }
  viewAllSubCategory() {
    this.productService.getAllSubCategory(this.id).subscribe(data => {
    this.mainCategory = data;
    }, err => {
      console.log(err);
    });
  }
  viewSingleMainCategory() {
    this.productService.getAllMainCategory(this.id, this.cat.mainCatId).subscribe(data => {
      this.mainCategory = data;
    }, err => {
      console.log(err);
    });
  }

  viewAllColors() {
    this.productService.getFilterColors().subscribe(data => {
      this.color = data;
      /* this.getQueryParams(); */
    }, err => {
      console.log(err);
    });
  }
  toggleFilter() {
    this.showMobileView = !this.showMobileView;
  }
  openModal(){

          this.display='block';
    
       }
       onCloseHandled(){

            this.display='none';
      
         }
}

