import { Observable, of } from 'rxjs';
import { ProductService } from '../../product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';

@Injectable()
export class AllAttributeResolver implements Resolve<Product> {
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> {
   /*  const supId = route.paramMap.get('catid');
    return this.productService.getSuperCategoryAttributeFilter(supId); /* .pipe(
      catchError(_ => {
        this.router.navigate(['']);
        return of(new Product());
      })
    ); */
    
    const supId = route.paramMap.get('catid');
    const subId = route.firstChild.paramMap.get('subid');
    console.log(subId, supId, 'test 1')
    if(subId) {
    return this.productService.getSubCategoryAttribute(subId);
  } else {
    return this.productService.getSuperCategoryAttribute(supId);
  } /* .pipe(
      catchError(_ => {
        this.router.navigate(['']);
        return of(new Product());
      })
    ); */
  }
}

