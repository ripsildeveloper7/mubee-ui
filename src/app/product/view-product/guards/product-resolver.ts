import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ProductService } from './../../../product/product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';

@Injectable()
export class ProductResolver implements Resolve<Product> {
  paginatorPages:{
    pageNo:number;
    perPage:number;
  };
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> {
    const catId = route.paramMap.get('catid');
    if(route.queryParamMap.get('pageNo')){
    
      const pageNumber = parseInt(route.queryParamMap.get('pageNo'));
      // const perPages = parseInt(route.queryParamMap.get('perPage'));
      this.paginatorPages = {
        pageNo:pageNumber,
        perPage:30,
      }
    }else{
        this.paginatorPages = {
          pageNo : 1,
          perPage :30,
        }
    }
    return this.productService.getAllSuperCategoryProduct(catId,this.paginatorPages); 
    /* .pipe(
      
      catchError(_ => {
        this.router.navigate(['']);
        return of(new Product());
      })
    ); */
  }
}

