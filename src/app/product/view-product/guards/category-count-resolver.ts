import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ProductService } from './../../../product/product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';

@Injectable()
export class CategoryCountResolver implements Resolve<Product> {
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> {
    const supId = route.paramMap.get('catid');
    const subId = route.firstChild.paramMap.get('subid');
    const mainId = route.firstChild.paramMap.get('maincatid');
    if (subId) {
        return this.productService.getSubCatCount(subId);
    } else if(mainId) {
        return this.productService.getMainCatCount(mainId);
    } else {
        return this.productService.getSupCatCount(supId);
    }
    
  }
}
