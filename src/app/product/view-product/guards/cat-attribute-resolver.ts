import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ProductService } from './../../../product/product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';

@Injectable()
export class CatAttributeResolver implements Resolve<Product> {
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> {
    const supId = route.paramMap.get('catid');
    const subId = route.firstChild.paramMap.get('subid');
    const mainId = route.firstChild.paramMap.get('maincatid');
    if (supId && mainId && subId) {
        return this.productService.getSubCategoryWithFilter(subId);
    } else if(supId && mainId) {
        return this.productService.getMainCategoryWithFilter(mainId);
    } else {
        return this.productService.getCategoryWithFilter(supId);
    }
    
  }
}

