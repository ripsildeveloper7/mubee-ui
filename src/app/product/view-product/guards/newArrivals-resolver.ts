import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ProductService } from '../../product.service';
import { Injectable, ViewChild, AfterViewInit, Input } from '@angular/core';
import {
    Resolve,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router,
    ActivatedRoute
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';


@Injectable()
export class NewArriavalResolver implements Resolve<Product> {
    @Input() arrayModel: any;
    constructor(
        private productService: ProductService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<Product> {
        const value = route.paramMap.get('num');
        const holder: any = new Product();
        holder.promotionProudctId = route.queryParams.array;
        console.log(value);
        return this.productService.getPromotionProduct(holder);
    }
}

