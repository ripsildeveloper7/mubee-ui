import { Observable, of } from 'rxjs';
import { ProductService } from '../../product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';

@Injectable()
export class AttributeProductResolver implements Resolve<Product> {
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> {
    const supId = route.parent.paramMap.get('catid');
    const attributeId = route.paramMap.get('attributeId');
    const attributefieldId = route.paramMap.get('attributefieldId');
    return this.productService.getProductAttribute(supId, attributeId, attributefieldId); /* .pipe(
      catchError(_ => {
        this.router.navigate(['']);
        return of(new Product());
      })
    ); */
  }
}

