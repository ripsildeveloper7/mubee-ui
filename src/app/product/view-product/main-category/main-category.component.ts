import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProductService } from '../../product.service';
import { AppSetting } from '../../../config/appSetting';

@Component({
  selector: 'app-main-category',
  templateUrl: './main-category.component.html',
  styleUrls: ['./main-category.component.css']
})
export class MainCategoryComponent implements OnInit {
  id: string;
  holder: any;
  superCat: any;
  imageUrl: string;
  superCatUrl: string;
  superCatID: any;
  superCatImg: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
              private productService: ProductService) {
    // this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
    //   this.id = params.get('id');
    // });
    this.imageUrl = AppSetting.mainCategoryBannerImageUrl;
    this.superCatUrl = AppSetting.categoryImageUrl;
  }

  ngOnInit() {
    // this.getCategory();
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.getCategory();
    });
  }
  getCategory() {
    this.productService.getSingleCategory(this.id).subscribe(data => {
      console.log(data);
      this.superCatID = data._id;
      this.superCatImg = data.categoryImageName;
      console.log( this.superCatImg);
      this.holder = data.mainCategory;
      console.log('main', this.holder);
    }, error => {
      console.log(error);
    });
  }
  /* getProduct(minId) {
    this.router.navigate(['product//maincategory']);
  } */
}
