import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Price } from './../filter-options/price.model';
import { Dispatch } from './../filter-options/dispatch.model';
import { CommonFilter } from './../../all-product/commonFilter.model';
@Component({
  selector: 'app-filter-options',
  templateUrl: './filter-options.component.html',
  styleUrls: ['./filter-options.component.css']
})
export class FilterOptionsComponent implements OnInit {
  @Input() filterOptionsModel: any;
  @Output() filterOptionData = new EventEmitter<CommonFilter>();
  @Input() commonFilter: CommonFilter;
  priceModel: Price;
  discount: any;
  dispatch: Dispatch;
  openDiv = false;
  filter1=false;
  filter2=false;
  filter3=false;
  constructor() {
    
   }

  ngOnInit() {
  }
  priceFilter(min, max){
    
    this.commonFilter.minPrice = min;
    this.commonFilter.maxPrice = max;
    this.filterOptionData.emit(this.commonFilter);
}
  discountFilter(min) {
    
    this.discount = min;
    this.commonFilter.discount = this.discount;
    this.filterOptionData.emit(this.commonFilter);
  }
  dispatchFilter(day){
    
    this.commonFilter.dispatch = day;
    this.filterOptionData.emit(this.commonFilter);
  }
  openAccordianOne() {
  this.filter1 = !this.filter1
  }
  
  openAccordianTwo() {
    this.filter2 = !this.filter2
    
  }
  
  openAccordianThree() {
    this.filter3 = !this.filter3
  
  }
}
