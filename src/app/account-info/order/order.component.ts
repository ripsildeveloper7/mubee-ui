import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Order } from './../../shared/model/order.model';
import { AppSetting } from '../../config/appSetting';
import { Router } from '@angular/router';
import { ReviewProductService } from '../review-product/review-product.service';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  userId: string;
  orderModel: any;
  productImageUrl: string = AppSetting.productImageUrl;
  checkStatus: any;
  productId: any;
  constructor(private accountService: AccountService, private router: Router,
              private  reviewProductService: ReviewProductService ) { }

  ngOnInit() {
    this.getOrders();
  }

  getOrders() {
    this.userId = sessionStorage.getItem('userId');
    this.accountService.getCustomerOrderDetails(this.userId).subscribe(data => {
      this.orderModel = data;
      this.checkStatus = data[0];
    }, error => {
      console.log(error);
    });
  }
  viewProduct(id) {
    this.router.navigate(['product/viewsingle/', id]);
  }
  createReview(id) {
    this.reviewProductService.productReview(id).subscribe(
      res => {
        if (res)  {
          this.getOrders();
        }
      }
    );

  }
  trackTheOrder(awbNo, orderId) {
    this.router.navigate(['account/trackorders/', awbNo, 'order', orderId]);
    /* this.accountService.trackAWB(awbNo).subscribe(data => {
      console.log(data);
    }, err => {
      console.log('err', err);
    }); */
  }
}
