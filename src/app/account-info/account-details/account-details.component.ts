import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared/shared.service';


@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {

  profiledetails = [ {name: 'Profile' , link: '/account/profile'},
  {name: 'Address' , link: '/account/listaddress'},
  {name: 'Order Details' , link: '/account/orders'},
  {name: 'Wishlist', link:'/wishlist/user'}
];
displayMobile = false;
  active= false;
  constructor(public sharedService: SharedService) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.displayMobile = false;
    } else {
      this.displayMobile = true;
    }
  }

  activate(){
    this.active = true;
  }

  

}
