import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { SignIn } from '../signin/signIn.model';
import { AccountService } from '../account.service';
import { Cart } from './../../shared/model/cart.model';
import { Product } from './../../shared/model/product.model';
import { RegModel } from '../registration/registration.model';
import { MustMatch } from './must-match.validator';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  passwordForm: FormGroup;
  accountDetail;
  userId;
  holder;
  showError = false;
  constructor(private fb: FormBuilder, private router: Router, private accountService: AccountService) { }

  ngOnInit() {
    this.userId = sessionStorage.getItem('userId');
    this.createForm();
  }
  createForm() {
    this.passwordForm = this.fb.group({
      oldPwd: ['', Validators.required],
      newPwd: ['', Validators.required],
      confirmPwd: ['', Validators.required]
    },
    {
      validator: MustMatch('newPwd', 'confirmPwd')
    });
  }
  updateResetPassword() {
    this.holder = new RegModel();
    this.holder.oldPwd = this.passwordForm.controls.oldPwd.value;
    this.holder.password = this.passwordForm.controls.newPwd.value;
    this.accountService.updatePassword(this.holder, this.userId).subscribe(data => {
      console.log(data);
      if (data.result) {
        this.showError = true;
      } else {
        this.showError = false;
        this.router.navigate(['account/profile']);
      }
    }, error => {
      console.log(error);
    });
}
cancel() {
  this.router.navigate(['account/profile']);
}
}
