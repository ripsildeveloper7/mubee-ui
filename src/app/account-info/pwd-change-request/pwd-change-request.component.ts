import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AccountService } from '../account.service';
import { Router } from '@angular/router';
import { PwdChangeRequest } from './pwd-change-request.model';

@Component({
  selector: 'app-pwd-change-request',
  templateUrl: './pwd-change-request.component.html',
  styleUrls: ['./pwd-change-request.component.css']
})
export class PwdChangeRequestComponent implements OnInit {
  passwordRequestForm: FormGroup;
  userModel: PwdChangeRequest;
  resetemailsent: boolean = false;
  emailmismatch: boolean = false;
  error: boolean = false;

  public state = 'inactive';

  public loadState  = 'load';

  constructor(private fb: FormBuilder, private accountService: AccountService, private router: Router,
   ) {

    }

  ngOnInit() {
    // this.createForm();
  }
  // createForm() {
  //   this.passwordRequestForm = this.fb.group({
  //     emailId: ['', Validators.required]
  //   });
  // }
  // toggleState() {
  //   this.state = this.state === 'active' ? 'inactive' : 'active';
  // }


  // sendResetSubmit(passwordRequestForm: FormGroup) {
  //   this.userModel = new PwdChangeRequest(
  //     passwordRequestForm.controls.emailId.value
  //   );

  //   this.accountService.pwdRequest(this.userModel).subscribe(serviceResult => {
  //     switch (serviceResult.result) {
  //       case 0: {
  //         this.error = true;
  //         break;
  //       }
  //       case 1: {
  //         this.resetemailsent = true;
  //         break;
  //       }
  //       case 2: {
  //         this.emailmismatch = true;
  //         break;
  //       }
  //       default: {
  //         this.error = true;
  //         break;
  //       }
  //     }
  //   }, error => {
  //     console.log(error);
  //   });
  // }

}
