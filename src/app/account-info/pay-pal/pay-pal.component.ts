import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
declare var paypal;
@Component({
  selector: 'app-pay-pal',
  templateUrl: './pay-pal.component.html',
  styleUrls: ['./pay-pal.component.css']
})
export class PayPalComponent implements OnInit {
@ViewChild('paypal', {static: true}) paypalElement: ElementRef;
product = {
  price: 777,
  decription: 'recent',
  img: 'assets/images/Icons/paytm.png'
}
  constructor() { }

  ngOnInit() {
    paypal.Buttons({
      createOrder: (data, actions) =>{
        return actions.order.create({
          purchase_units:[{
            description: this.product.decription, 
            amount: {
              currency_code: 'USD',
              value: this.product.price
            }
          }
          ]
        })
      }
    }).render(this.paypalElement.nativeElement)
  }

}
