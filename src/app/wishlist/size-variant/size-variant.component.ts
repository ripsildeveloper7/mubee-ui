
import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { WishList } from './../../shared/model/wishList.model';
import { WishlistService } from './../wishlist.service';
import { SelectService } from '../view-wishlist/select.service';

@Component({
  selector: 'app-size-variant',
  templateUrl: './size-variant.component.html',
  styleUrls: ['./size-variant.component.css']
})
export class SizeVariantComponent implements OnInit {
  wishForm: FormGroup;
  wishList: WishList[];
  wish: WishList;
  userId: string;
  selectedSize: boolean = false;
  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<SizeVariantComponent>, private wishlistService: WishlistService,  private fb: FormBuilder) { }

  ngOnInit() {
    this.wishForm = this.fb.group({
      seletedSku: ['']
    });
  }

  
  selectedWishSize(data, wish) {
    console.log(data);
      
    wish.productIds.INTsku = data.INTsku;
   /*  wish.forEach(mat => {
      if (mat.productIds.proId === wish.productIds.proId) {
          mat.productIds.sku = data;
      }
     }); */
  }
  moveToBag(proId, pro){
    if (JSON.parse(sessionStorage.getItem('login')) && pro.INTsku) {
      this.selectedSize = false;
      this.userId = sessionStorage.getItem('userId');
      this.wish = new WishList();
      this.wish.userId = this.userId;
      this.wish.productId = proId;
      this.wish.INTsku = pro.INTsku;
      this.wishlistService.moveToAddtoCart(this.wish).subscribe(data => {
        this.wishList = data;
      
        /* 
        const wishlist: any = this.wishList.map(a => a.productIds);
        sessionStorage.setItem('wislistLength', wishlist.length); */
        this.dialogRef.close(true);
        }, err => {
          console.log(err);
        });
      }
     else {
      this.selectedSize = true;
    }
}
}
