import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AppSetting } from '../config/appSetting';
import { WishList } from '../shared/model/wishList.model';


@Injectable({
  providedIn: 'root'
})
export class WishlistService {
  productServiceUrl: string = AppSetting.productServiceUrl;
  marketingServiceUrl: string = AppSetting.marketingServiceUrl;
  constructor(private httpClient: HttpClient) { }
  /* wishlist  start */
  getWishList(wish): Observable<WishList[]> {
    const pathUrl = 'getwishlistmvp/';
    const url: string = this.productServiceUrl + pathUrl + wish.userId;
    return this.httpClient.get<WishList[]>(url);
  }
  addToWishList(wish): Observable<WishList[]> {
    const pathUrl = 'wishlistmvp/';
    const url: string = this.productServiceUrl + pathUrl + wish.userId;
    return this.httpClient.put<WishList[]>(url, wish);
  }
  moveToAddtoCart(wish): Observable<WishList[]> {
    const pathUrl = 'movetoaddtocartmvp/';
    const url: string = this.productServiceUrl + pathUrl + wish.userId;
    return this.httpClient.put<WishList[]>(url, wish);
  }
  deleteWishlist(userId, wishid): Observable<WishList[]> {
    const pathUrl = 'wishlistmvp/';
    const url: string = this.productServiceUrl + pathUrl + userId + '/' + wishid;
    return this.httpClient.delete<WishList[]>(url);
  }
  /* wishlist  end */
  getAllDiscount(): Observable<any> {
    const pathUrl = 'getdiscount';
    const url: string = this.marketingServiceUrl + pathUrl;
    return this.httpClient.get<any>(url);
  }
  getCartCountUserId(userId): Observable<any> {
    const pathUrl = 'findcartcount/';
    const url: string = this.marketingServiceUrl + pathUrl + userId;
    return this.httpClient.get<any>(url);
  }
}
