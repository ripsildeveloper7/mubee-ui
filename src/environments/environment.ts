// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  // productServiceUrl: "http://localhost:3072/",
  // contentServiceUrl: "http://localhost:3110/",
  contentServiceUrl: 'https://hos0hp8j8e.execute-api.ap-south-1.amazonaws.com/qa/',
  instagramUrl: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=8636464595.f5aef9d.b112bf6c1e6a41a482ec7e7a77882469',
  productServiceUrl: 'https://euwinsi5x6.execute-api.ap-south-1.amazonaws.com/qa/',  
  productImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/product/',
  sizeGuideImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/size/',
  commerceOrderServiceUrl: "http://localhost:3112/",
  // commerceOrderServiceUrl: 'https://m317jtnwx8.execute-api.ap-south-1.amazonaws.com/qa/',
  // customerServiceUrl: "http://localhost:3111/",
  customerServiceUrl: 'https://3mek1zbxme.execute-api.ap-south-1.amazonaws.com/qa/',
  subCategoryImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/subcategory/',
  categoryImageUrl: "https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/category/",
  brandImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/brand/',
  marketingServiceUrl: 'https://7b36xythn5.execute-api.ap-south-1.amazonaws.com/qa/',
  mainCategoryBannerImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/maincategory/',
  categoryBannerImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/categorybanner/',
  // excelUrl:  'https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/excel/',
  // imageUploadServiceUrl:  'https://ha0h1va6a0.execute-api.ap-south-1.amazonaws.com/qa/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
