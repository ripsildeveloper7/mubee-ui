

export const environment = {
  production: true,
  contentServiceUrl: 'https://hos0hp8j8e.execute-api.ap-south-1.amazonaws.com/qa/',
  instagramUrl: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=8636464595.f5aef9d.b112bf6c1e6a41a482ec7e7a77882469',
  productServiceUrl: 'https://euwinsi5x6.execute-api.ap-south-1.amazonaws.com/qa/',
  productImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/product/',
  sizeGuideImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/size/',
  commerceOrderServiceUrl: 'https://m317jtnwx8.execute-api.ap-south-1.amazonaws.com/qa/',
  customerServiceUrl: 'https://3mek1zbxme.execute-api.ap-south-1.amazonaws.com/qa/',
  subCategoryImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/subcategory/',
  categoryImageUrl: "https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/category/",
  brandImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/brand/',
  marketingServiceUrl: 'https://7b36xythn5.execute-api.ap-south-1.amazonaws.com/qa/',
  categoryBannerImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/categorybanner/',
  mainCategoryBannerImageUrl: 'https://mubee-product-images.s3.ap-south-1.amazonaws.com/images/maincategory/',
  // excelUrl:  'https://studentbus.in-product-images.s3.ap-south-1.amazonaws.com/excel/',
  // imageUploadServiceUrl:  'https://ha0h1va6a0.execute-api.ap-south-1.amazonaws.com/qa/',
};
